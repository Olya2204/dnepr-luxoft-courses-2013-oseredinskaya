package com.luxoft.courses.javaWeb.listener;


import com.luxoft.courses.javaWeb.entity.User;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class UsersListener implements ServletContextListener {
    private static List<User> listUsers = new ArrayList<>();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String xml = sce.getServletContext().getInitParameter("users");
        InputStream is = sce.getServletContext().getResourceAsStream("META-INF/" + xml);
        try {
            DOMParser parser = new DOMParser();
            parser.parse(new InputSource(is));
            Document doc = parser.getDocument();

            NodeList users = doc.getElementsByTagName("user");

            for (int i = 0; i < users.getLength(); i++) {
                saveAttributes(users.item(i));
            }

        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private void saveAttributes(Node user) {
        NamedNodeMap attributes = user.getAttributes();

        String name = attributes.getNamedItem("name").getNodeValue();
        String password = attributes.getNamedItem("password").getNodeValue();
        listUsers.add(new User(name, password));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    public static List<User> getListUsers() {
        return listUsers;
    }
}
