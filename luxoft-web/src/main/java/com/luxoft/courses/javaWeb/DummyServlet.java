package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DummyServlet extends HttpServlet {
    private static final Map<String, Integer> parameters = new ConcurrentHashMap<>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String age = request.getParameter("age");

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        if (name == null || age == null) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("{\"error\": \"Illegal parameters\"}");
            return;
        }

        if (parameters.containsKey(name)) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("{\"error\": \"Name " + name + " already exists\"}");
        } else {
            parameters.put(name, Integer.parseInt(age));
            response.setStatus(HttpServletResponse.SC_CREATED);
        }
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getHeader("name");
        String age = request.getHeader("age");

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        if (name == null || age == null) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("{\"error\": \"Illegal parameters\"}");
            return;
        }

        if (parameters.containsKey(name)) {
            parameters.put(name, Integer.parseInt(age));
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("{\"error\": \"Name " + name + " does not exist\"}");
        }
    }
}
