<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<b style="color: blue">Please Enter your Login and Password:</b><br><br>

<%!
    public String printError(HttpServletRequest request) {
        if (request.getAttribute("error") == null) return "";
        return (Boolean) request.getAttribute("error") ? "Wrong username or password. Please try again" : "";
    }
%>

<form name="form" method="post" action="login">
    <table border="0">
        <tr align="left" align="top">
            <td>Login:</td>
            <td><input type="text" name="name"/></td>
            <td><%=printError(request)%>
            </td>
        </tr>
        <tr align="left" align="top">
            <td>Password:</td>
            <td><input type="password" name="password"/></td>
        </tr>
        <tr align="left" align="top">
            <td></td>
            <td><input type="submit" value="Sign in"/></td>
        </tr>
    </table>
</form>
</body>
</html>


