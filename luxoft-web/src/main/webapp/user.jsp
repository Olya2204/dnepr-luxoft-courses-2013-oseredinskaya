<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    if (request.getSession().getAttribute("name") == null) {
        response.sendRedirect("");
    }
%>

<html>
<head>
    <title></title>
</head>
<body>

<a href="logout" align="right">logout</a>

<form name="frm" method="get" action="user">

    <h3>
        Hello, <%=(String) request.getSession().getAttribute("name")%>!
    </h3>

</form>
</body>
</html>