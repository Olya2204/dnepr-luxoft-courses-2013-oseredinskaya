package com.luxoft.dnepr.courses.unit3.controller;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import static org.junit.Assert.*;

public class DeckTest {

    @Test
    public void testCreate() {
        try {
            Deck.createDeck(3);
            Assert.fail("3 must invoke IllegalArgumentException within createDeck(int size) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Number of decks should be 1, 2, 4, 6, 8 or 10.", e.getMessage());
        }
        assertEquals(52, Deck.createDeck(1).size());
        assertEquals(104, Deck.createDeck(2).size());
        assertEquals(208, Deck.createDeck(4).size());
        assertEquals(52 * 10, Deck.createDeck(10).size());

        assertEquals(52 * 10, Deck.createDeck(11).size());
        assertEquals(52, Deck.createDeck(-1).size());

        List<Card> deck = Deck.createDeck(2);
        int i = 0;

        for (int deckN = 0; deckN < 2; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    assertEquals(suit, deck.get(i).getSuit());
                    assertEquals(rank, deck.get(i).getRank());
                    assertEquals(rank.getCost(), deck.get(i).getCost());
                    i++;
                }
            }
        }
    }

    @Test
    public void testCostOf() {
        try {
            Deck.costOf(null);
            Assert.fail("null must invoke IllegalArgumentException within costOf(List<Card> hand) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Transfer Null value to the method.", e.getMessage());
        }

        List<Card> hand = new LinkedList<Card>();
        hand.add(new Card(Rank.RANK_10, Suit.SPADES));
        hand.add(new Card(Rank.RANK_ACE, Suit.SPADES));
        hand.add(new Card(Rank.RANK_JACK, Suit.DIAMONDS));
        hand.add(new Card(Rank.RANK_2, Suit.CLUBS));
        assertEquals(33, Deck.costOf(hand));
        assertEquals(380, Deck.costOf(Deck.createDeck(1)));

    }
}
