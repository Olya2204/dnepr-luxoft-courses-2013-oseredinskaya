package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class GameController {
    private static final int MIN_DEALER_COST = 17;

    private static final int MAXIMUM_COST = 21;

    private static GameController controller;

    private List<Card> deck;
    private Iterator<Card> deckIterator;

    private List<Card> myHand;
    private List<Card> dealersHand;

    private GameController() {
        deck = Deck.createDeck(1);
        myHand = new ArrayList<Card>();
        dealersHand = new ArrayList<Card>();
        deckIterator = deck.iterator();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    void newGame(Shuffler shuffler) {
        myHand.clear();
        dealersHand.clear();
        shuffler.shuffle(deck);
        deckIterator = deck.iterator();

        myHand.add(deckIterator.next());
        myHand.add(deckIterator.next());

        dealersHand.add(deckIterator.next());
    }

    public boolean requestMore() {
        if (deckIterator == null) {
            return false;
        }
        if (Deck.costOf(myHand) > MAXIMUM_COST) {
            return false;
        }
        myHand.add(deckIterator.next());
        return Deck.costOf(myHand) <= MAXIMUM_COST;
    }

    public void requestStop() {
        while (Deck.costOf(dealersHand) < MIN_DEALER_COST) {
            dealersHand.add(deckIterator.next());
        }
    }

    public WinState getWinState() {
        int myCost = Deck.costOf(myHand);
        if (myCost > MAXIMUM_COST) {
            return WinState.LOOSE;
        }
        int dealersCost = Deck.costOf(dealersHand);
        if (dealersCost > MAXIMUM_COST || myCost > dealersCost) {
            return WinState.WIN;
        }
        if (dealersCost > myCost) {
            return WinState.LOOSE;
        }
        return WinState.PUSH;
    }

    public List<Card> getMyHand() {
        return Collections.unmodifiableList(myHand);
    }

    public List<Card> getDealersHand() {
        return Collections.unmodifiableList(dealersHand);
    }
}
