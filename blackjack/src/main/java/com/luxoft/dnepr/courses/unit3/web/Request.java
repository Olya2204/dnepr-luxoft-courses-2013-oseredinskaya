package com.luxoft.dnepr.courses.unit3.web;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Request {

    private final String method;
    private final String uri;
    private final String body;
    private final Map<String, String> parameters;

    Request(String method, String uri, Reader reader) throws IOException {
        this.method = method;
        if (method != null && method.equalsIgnoreCase("GET")) {
            int index = uri.indexOf('?');
            this.uri = index < 0 ? uri : uri.substring(0, index);
            this.body = "";
            this.parameters = parseParameters(uri.substring(index + 1));
        } else {
            this.uri = uri;
            this.body = readFully(reader);
            this.parameters = parseParameters(body);
        }
    }

    private Map<String, String> parseParameters(String body) {
        Map<String, String> result = new HashMap<String, String>();
        int index = 0;
        while (index < body.length()) {
            index = acceptParameter(body, result, index);
        }
        return Collections.unmodifiableMap(result);
    }

    private int acceptParameter(String body,
                                Map<String, String> result, int index) {
        int startValue = body.indexOf('=', index);
        if (startValue < 0) {
            result.put(body, "");
            return body.length();
        }
        String name = body.substring(index, startValue);
        startValue++;

        int endValue = body.indexOf("&", startValue);
        if (endValue < 0) {
            result.put(name, body.substring(startValue));
            return body.length();
        }
        String value = body.substring(startValue, endValue);

        result.put(name, value);
        return endValue + 1;
    }

    private String readFully(Reader reader) throws IOException {
        if (!reader.ready()) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        char[] buffer = new char[1024];
        int size;
        while ((size = reader.read(buffer)) > 0) {
            result.append(buffer, 0, size);
        }
        return result.toString();
    }

    public String getMethod() {
        return method;
    }

    public String getUri() {
        return uri;
    }

    public Reader getReader() {
        return new StringReader(body);
    }

    public Map<String, String> getParameters() {
        return parameters;
    }
}
