package com.luxoft.dnepr.courses.unit3.model;

public enum Rank {

    RANK_ACE(11, "A"),
    RANK_2(2, "2"),
    RANK_3(3, "3"),
    RANK_4(4, "4"),
    RANK_5(5, "5"),
    RANK_6(6, "6"),
    RANK_7(7, "7"),
    RANK_8(8, "8"),
    RANK_9(9, "9"),
    RANK_10(10, "10"),
    RANK_JACK(10, "J"),
    RANK_QUEEN(10, "Q"),
    RANK_KING(10, "K");


    private int cost;
    private String name;

    private Rank(int cost, String name) {
        this.name = name;
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

}
