package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;
    private static final String COMMAND_HELP = Command.HELP;
    private static final String COMMAND_MORE = Command.MORE;
    private static final String COMMAND_STOP = Command.STOP;
    private static final String COMMAND_EXIT = Command.EXIT;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: Olga Seredinskaya\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();
        controller.newGame();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see //com.luxoft.dnepr.courses.unit3.view.CommandLineInterfaceTest
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {

        switch (command) {
            case COMMAND_HELP:
                executeHelp();
                break;
            case COMMAND_MORE:
                return executeMore(controller);
            case COMMAND_STOP:
                executeStop(controller);
                break;
            case COMMAND_EXIT:
                return false;
            default:
                output.println("Invalid command");
        }

        return true;
    }

    private void executeHelp() {
        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private boolean executeMore(GameController controller) {
        controller.requestMore();
        printState(controller);
        if (controller.getWinState().equals(WinState.LOOSE)) {
            output.println();
            output.println("Sorry, today is not your day. You loose.");
            return false;
        }
        return true;
    }

    private void executeStop(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:\n");

        printState(controller);
        output.println();

        if (controller.getWinState().equals(WinState.LOOSE)) {
            output.println("\tSorry, today is not your day. You loose.");
        }
        if (controller.getWinState().equals(WinState.WIN)) {
            output.println("\tCongrats! You win!");
        }
        if (controller.getWinState().equals(WinState.PUSH)) {
            output.println("\tPush. Everybody has equal amount of points.");
        }

    }


    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        StringBuilder bufMyHand = new StringBuilder();
        StringBuilder bufDealersHand = new StringBuilder();

        for (Card cards : myHand) {

            bufMyHand.append(cards.getRank().getName()).append(" ");
        }


        List<Card> dealersHand = controller.getDealersHand();
        for (Card cards : dealersHand) {

            bufDealersHand.append(cards.getRank().getName()).append(" ");
        }

        output.println("\t" + bufMyHand + "(total " + Deck.costOf(myHand) + ")\n" +
                "\t" + bufDealersHand + "(total " + Deck.costOf(dealersHand) + ")");

    }
}
