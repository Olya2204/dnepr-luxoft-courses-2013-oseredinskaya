package com.luxoft.dnepr.courses.unit3.controller;

import java.util.LinkedList;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

public final class Deck {
    private static final String NULL_VALUE_ERROR_MESSAGE = "Transfer Null value to the method.";
    private static final String DECK_SIZE_ERROR_MESSAGE = "Number of decks should be 1, 2, 4, 6, 8 or 10.";

    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        List<Card> cards = new LinkedList<Card>();
        if (size > 10) {
            size = 10;
        }
        if (size < 1) {
            size = 1;
        }

        if ((size > 1) && !(size % 2 == 0)) {
            throw new IllegalArgumentException(DECK_SIZE_ERROR_MESSAGE);
        }

        for (int i = 1; i <= size; i++) {
            createOneDeck(cards);
        }
        return cards;

    }

    private static List<Card> createOneDeck(List<Card> cards) {
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cards.add(new Card(rank, suit));
            }
        }
        return cards;
    }

    public static int costOf(List<Card> hand) {
        if (hand == null) {
            throw new IllegalArgumentException(NULL_VALUE_ERROR_MESSAGE);
        }
        int S = 0;
        for (Card card : hand) {
            S = S + card.getCost();
        }
        return S;
    }
}
