package com.luxoft.dnepr.courses.unit3.web;

import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class MethodDispatcher {
    private static final String WIN_MESSAGE = "Congrats! You win!";
    private static final String LOOSE_MESSAGE = "Sorry, today is not your day.You loose.";
    private static final String PUSH_MESSAGE = "Push. Everybody has equal amount of points.";

    /**
     * @param request
     * @param response
     * @return response or <code>null</code> if wasn't able to find a method.
     */
    public Response dispatch(Request request, Response response) {
        String method = request.getParameters().get("method");
        if (method == null) {
            return null;
        }
        if (method.equals("Hit")) {
            return requestMore(response);
        }
        if (method.equals("NewGame")) {
            return requestNewGame(response);
        }
        if (method.equals("Stop")) {
            return requestStop(response);
        }
        return null;
    }

    private Response requestMore(Response response) {
        response.write("{\"res\":" + GameController.getInstance().requestMore());
        response.write(",\"myHand\":");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write(",\"winState\":");
        response.write("{\"state\":\"" + getState() + "\"}");
        response.write("}");
        return response;
    }

    private Response requestStop(Response response) {
        GameController.getInstance().requestStop();
        response.write("{\"myHand\":");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write(",");
        response.write("\"dealersHand\":");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write(",\"winState\":");
        response.write("{\"state\":\"" + getState() + "\"}");
        response.write("}");
        return response;
    }

    private Response requestNewGame(Response response) {
        GameController.getInstance().newGame();
        response.write("{\"myHand\":");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write(",");
        response.write("\"dealersHand\":");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write(",\"winState\":");
        if (Deck.costOf(GameController.getInstance().getMyHand()) == 21 && (Deck.costOf(GameController.getInstance().getDealersHand()) != 10)) {
            response.write("{\"state\":\"" + getState() + "\"}");
        } else {
            response.write("{\"state\":\"\"}");
        }
        response.write("}");
        return response;
    }


    private void writeHand(Response response, List<Card> hand) {
        boolean isFirst = true;
        response.write("{\"cards\":[");
        for (Card card : hand) {
            if (isFirst) {
                isFirst = false;
            } else {
                response.write(",");
            }
            response.write("{\"rank\":\"");
            response.write(card.getRank().getName());
            response.write("\",\"suit\":\"");
            response.write(card.getSuit().name());
            response.write("\",\"cost\":\"");
            response.write(String.valueOf(card.getCost()));
            response.write("\"}");

        }
        response.write("]}");
    }

    private String getState() {
        if (GameController.getInstance().getWinState().equals(WinState.LOOSE)) {
            return LOOSE_MESSAGE;
        } else {
            if (GameController.getInstance().getWinState().equals(WinState.WIN)) {
                return WIN_MESSAGE;
            } else {
                if (GameController.getInstance().getWinState().equals(WinState.PUSH)) {
                    return PUSH_MESSAGE;
                } else {
                    return "";
                }
            }
        }
    }

}
