package com.luxoft.dnepr.courses.unit3.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Response {

    private final ByteArrayOutputStream body;
    private final String contextType;

    Response() {
        body = new ByteArrayOutputStream();
        contextType = "text/plain";
    }

    Response(InputStream file, String contextType) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int size;
        while ((size = file.read(buf)) > 0) {
            result.write(buf, 0, size);
        }
        this.body = result;
        this.contextType = contextType;
    }

    public void write(String data) {
        byte[] bytes = data.getBytes();
        body.write(bytes, 0, bytes.length);
    }

    byte[] getBody() {
        return body.toByteArray();
    }

    public String getContentType() {
        return contextType;
    }

}
