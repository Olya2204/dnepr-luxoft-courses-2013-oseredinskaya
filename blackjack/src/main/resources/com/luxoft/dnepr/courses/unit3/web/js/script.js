$(document).ready(function () {
    $.ajaxSetup({ cache: false });

    $("button#newGame").click(function () {
        beforeGame();
        $.getJSON("/service", {method: "NewGame"}, function (json) {
            for (var i in json.myHand.cards) {
                var myCard = "../deck/" + String(json.myHand.cards[i].rank) + String(json.myHand.cards[i].suit.charAt(0)) + ".png";
                $("<img/>").attr({src: myCard}).appendTo("#PlayersHand");
            }

            for (var j in json.dealersHand.cards) {
                var dCard = "../deck/" + String(json.dealersHand.cards[j].rank) + String(json.dealersHand.cards[j].suit.charAt(0)) + ".png";
                $("<img/>").attr("src", dCard).appendTo("#DealersHand");
            }

            var state = String(json.winState.state);
            if (state !== "") {
                getState(state);
            }
        });
    });

    $("button#hit").click(function () {
        $("#PlayersHand").empty();
        $.getJSON("/service", {method: "Hit"}, function (json) {
            for (var k in json.myHand.cards) {
                var myCard = "../deck/" + String(json.myHand.cards[k].rank) + String(json.myHand.cards[k].suit.charAt(0)) + ".png";
                $("<img/>").attr({src: myCard}).appendTo("#PlayersHand");
            }

            var res = json.res;
            if (res === false) {
                var state = String(json.winState.state);
                getState(state);
            }
        });

    });

    $("button#stop").click(function () {
        $("#DealersHand").empty();
        $.getJSON("/service", {method: "Stop"}, function (json) {
            for (var j in json.dealersHand.cards) {
                var myCard = "../deck/" + String(json.dealersHand.cards[j].rank) + String(json.dealersHand.cards[j].suit.charAt(0)) + ".png";
                $("<img/>").attr({src: myCard}).appendTo("#DealersHand");
            }
            var state = String(json.winState.state);
            getState(state);
        });
    });

    function beforeGame() {
        $("#PlayersHand").empty();
        $("#DealersHand").empty();
        $("#WinState").empty();
        $("button#hit").removeAttr("disabled");
        $("button#stop").removeAttr("disabled");
    }

    function getState(state) {
        $('#WinState').append('<p>' + state + '</p>');
        $("button#hit").attr("disabled", "disabled");
        $("button#stop").attr("disabled", "disabled");
    }

});