SELECT maker_id, AVG(screen) AS avg_size FROM laptop
JOIN product ON laptop.model = product.model
GROUP BY maker_id
ORDER BY avg_size;