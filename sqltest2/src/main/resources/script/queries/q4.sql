SELECT maker_name FROM product p, makers m, printer pr
WHERE (pr.model = p.model)
AND (p.maker_id = m.maker_id)
GROUP BY maker_name
ORDER BY maker_name DESC;