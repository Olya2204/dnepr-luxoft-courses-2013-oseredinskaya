SELECT maker_id, COUNT(model) AS mod_count
FROM product
WHERE type='pc'
GROUP BY maker_id 
HAVING COUNT(model) >= 3;