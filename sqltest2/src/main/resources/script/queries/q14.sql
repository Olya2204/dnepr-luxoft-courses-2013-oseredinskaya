SELECT m.maker_name, price
FROM printer pr, makers m, product p
WHERE (m.maker_id = p.maker_id) AND (p.model = pr.model)
AND (pr.color = 'y') 
AND pr.price = (SELECT MIN(price) FROM printer WHERE color = 'y') ;