SELECT  p.type, l.model, l.speed FROM laptop l, pc pc, product p
WHERE p.type = 'Laptop'
GROUP BY l.model
HAVING l.speed < MIN(pc.speed);