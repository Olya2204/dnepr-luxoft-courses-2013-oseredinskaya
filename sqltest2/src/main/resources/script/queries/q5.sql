SELECT pr.model, pr.price FROM product p, makers m, printer pr
WHERE (pr.model = p.model)
AND (p.maker_id = m.maker_id)
AND m.maker_name LIKE 'B'
UNION 
SELECT pc.model, pc.price FROM product p, makers m, pc pc
WHERE (pc.model = p.model)
AND (p.maker_id = m.maker_id)
AND m.maker_name LIKE 'B'
UNION 
SELECT l.model, l.price FROM product p, makers m, laptop l
WHERE (l.model = p.model)
AND (p.maker_id = m.maker_id)
AND m.maker_name LIKE 'B';