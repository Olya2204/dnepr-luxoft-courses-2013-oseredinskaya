SELECT p.maker_id, AVG(pc.hd) AS avg_hd
FROM product p, pc pc
WHERE p.model = pc.model AND
p.maker_id IN(SELECT maker_id FROM product WHERE type = 'Printer')
GROUP BY p.maker_id;