SELECT m.maker_name, l.speed FROM makers m, laptop l, product p
WHERE (l.model = p.model)
AND (p.maker_id = m.maker_id)
AND (l.hd >= 10.0);
