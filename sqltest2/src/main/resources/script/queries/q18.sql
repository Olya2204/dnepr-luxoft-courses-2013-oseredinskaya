SELECT model FROM (SELECT price, model FROM pc UNION
SELECT price, model FROM laptop UNION
SELECT price, model FROM printer) t
WHERE price = (SELECT MAX(price) FROM
(SELECT price FROM pc UNION SELECT price FROM laptop UNION
SELECT price FROM printer) t);