package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {


    private static final String EMPTY_STRING_ERROR_MESSAGE = "Parameter is empty string.";
    private static final String NULL_VALUE_ERROR_MESSAGE = "Parameter is null value.";
    private static final String INPUT_ERROR_MESSAGE = "Invalid input.";
    private static final String CLOSING_BRACKET_ERROR_MESSAGE = "The closing bracket can't be the first. Invalid input";
    private static final String NUMBER_BRACKETS_ERROR_MESSAGE = "Brackets are incorrectly placed.";

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        List<String> list = new LinkedList<String>();
        Stack<String> stack = new Stack<String>();
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        int opening_counter = 0;
        int closing_counter = 0;

        if (input == null) {
            throw new CompilationException(NULL_VALUE_ERROR_MESSAGE);
        }


        if (input.isEmpty()) {
            throw new CompilationException(EMPTY_STRING_ERROR_MESSAGE);
        }

        String s = input.replaceAll(" ", "");
        StringTokenizer st = new StringTokenizer(s, "+-*/()", true);

        while (st.hasMoreTokens()) {
            list.add(st.nextToken());
        }


        for (String aList : list) {

            if (checkString(aList)) {
                addCommand(result, VirtualMachine.PUSH, Double.parseDouble(aList));
            } else {
                switch (aList) {
                    case "*":
                    case "/":
                    case "+":
                    case "-":
                        while (getPriory(aList, stack)) {
                            popStack(result, stack.pop());
                        }
                        stack.push(aList);
                        break;
                    case "(":
                        opening_counter++;
                        stack.push(aList);
                        break;
                    case ")":
                        closing_counter++;
                        if (!stack.empty()) {
                            while (!stack.peek().equals("(")) {
                                popStack(result, stack.pop());
                            }
                            stack.removeElementAt(stack.size() - 1);
                            break;
                        } else {
                            throw new CompilationException(CLOSING_BRACKET_ERROR_MESSAGE);
                        }
                    default:
                        throw new CompilationException(INPUT_ERROR_MESSAGE);
                }
            }

        }

        if (opening_counter != closing_counter) {
            throw new CompilationException(NUMBER_BRACKETS_ERROR_MESSAGE);
        }

        Iterator<String> operator = stack.iterator();
        while (operator.hasNext()) {
            popStack(result, stack.pop());
        }

        addCommand(result, VirtualMachine.PRINT);

        return result.toByteArray();
    }


    private static void popStack(ByteArrayOutputStream result, String operator) {
        switch (operator) {
            case "*":
                addCommand(result, VirtualMachine.MUL);
                break;
            case "/":
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.DIV);
                break;
            case "+":
                addCommand(result, VirtualMachine.ADD);
                break;
            case "-":
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.SUB);
                break;
        }

    }

    private static boolean checkString(String string) {
        try {
            Double.parseDouble(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean getPriory(String operator, Stack<String> stack) {
        if (stack.size() == 0) return false;
        int priory1 = 0;
        int priory2 = 0;
        String top = stack.peek();
        switch (operator) {
            case "*":
            case "/":
                priory1 = 2;
                break;
            case "+":
            case "-":
                priory1 = 1;
                break;
        }

        switch (top) {
            case "*":
            case "/":
                priory2 = 2;
                break;
            case "+":
            case "-":
                priory2 = 1;
                break;
        }

        return priory1 <= priory2;

    }


    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
