package com.luxoft.dnepr.courses.compiler;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CompilerTest extends AbstractCompilerTest {
    private static final String EMPTY_STRING_ERROR_MESSAGE = "Parameter is empty string.";
    private static final String NULL_VALUE_ERROR_MESSAGE = "Parameter is null value.";
    private static final String INPUT_ERROR_MESSAGE = "Invalid input.";
    private static final String CLOSING_BRACKET_ERROR_MESSAGE = "The closing bracket can't be the first. Invalid input";
    private static final String NUMBER_BRACKETS_ERROR_MESSAGE = "Brackets are incorrectly placed.";

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");
    }

    @Test
    public void testComplex() {
        assertCompiled(0, "  (2 - 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, "  2 *3 + 2.5");
        assertCompiled(1, " (2+2)/(2+2)");
        assertCompiled(1, "  100 / 2 / 5 / 10");
        assertCompiled(14, "  2 + 2 * (2 + 2 * 2)");
        assertCompiled(51.3, "22.3 + 16 / 4 - 4 * (17 - 2 * 7 + 3) + 7 * (3 + 4)");

    }

    @Test
    public void testCompileError() {
        try {
            Compiler.compile("1 + a2@-");
            Assert.fail("Should throw an exception");
        } catch (CompilationException e) {
            assertEquals(INPUT_ERROR_MESSAGE, e.getMessage());
        }

        try {
            Compiler.compile("");
            Assert.fail("Should throw an exception");
        } catch (CompilationException e) {
            assertEquals(EMPTY_STRING_ERROR_MESSAGE, e.getMessage());
        }

        try {
            Compiler.compile("  )34*5+1");
            Assert.fail("Should throw an exception");
        } catch (CompilationException e) {
            assertEquals(CLOSING_BRACKET_ERROR_MESSAGE, e.getMessage());
        }

        try {
            Compiler.compile(null);
            Assert.fail("Should throw an exception");
        } catch (CompilationException e) {
            assertEquals(NULL_VALUE_ERROR_MESSAGE, e.getMessage());
        }

        try {
            Compiler.compile("((4+5)/3");
            Assert.fail("Should throw an exception");
        } catch (CompilationException e) {
            assertEquals(NUMBER_BRACKETS_ERROR_MESSAGE, e.getMessage());
        }

    }
}
