import com.luxoft.dnepr.courses.dao.AbstractDao;
import com.luxoft.dnepr.courses.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.model.Redis;
import com.luxoft.dnepr.courses.storage.CreateDB;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RedisDaoTest {

    @Before
    public void init() {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        CreateDB.create();
    }

    @Test
    public void testDaoForRedis() throws Exception {

        AbstractDao<Redis> redisDao = new RedisDaoImpl();
        Redis redis = new Redis();
        redis.setId(146);
        redis.setWeight(10);

        Redis otherRedis = new Redis();
        otherRedis.setId(0);
        otherRedis.setWeight(7);

        redisDao.save(redis);
        redisDao.save(otherRedis);

        assertTrue(redisDao.isIdExists(147));
        assertTrue(redisDao.isIdExists(146));

        redis.setWeight(20);
        redisDao.update(redis);
        assertTrue(redisDao.isSecondParameterExists(20));


        redisDao.delete(redis.getId());

        assertFalse(redisDao.isIdExists(146));
        otherRedis.setId(147);
        try {
            redisDao.save(otherRedis);
            Assert.fail("This id must invoke UserAlreadyExist exception within save method.");
        } catch (UserAlreadyExist e) {
            assertEquals("Entity with the same id(147) is already exist", e.getMessage());
        }
    }
}
