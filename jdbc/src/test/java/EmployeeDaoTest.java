import com.luxoft.dnepr.courses.dao.*;
import com.luxoft.dnepr.courses.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.model.Employee;
import com.luxoft.dnepr.courses.storage.CreateDB;
import org.junit.*;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EmployeeDaoTest {
    @Before
    public void init() {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        CreateDB.create();
    }

    @Test
    public void testDaoForEmployee() throws Exception {
        AbstractDao<Employee> employeeDao = new EmployeeDaoImpl();
        Employee employee = new Employee();
        employee.setId(14566);
        employee.setSalary(1000);

        Employee otherEmployee = new Employee();
        otherEmployee.setId(0);
        otherEmployee.setSalary(7000);

        employeeDao.save(employee);
        employeeDao.save(otherEmployee);

        assertTrue(employeeDao.isIdExists(14567));
        assertTrue(employeeDao.isIdExists(14566));

        employee.setSalary(2000);
        employeeDao.update(employee);
        assertTrue(employeeDao.isSecondParameterExists(2000));

        employeeDao.delete(employee.getId());

        assertFalse(employeeDao.isIdExists(14566));
        otherEmployee.setId(14567);
        try {
            employeeDao.save(otherEmployee);
            Assert.fail("This id must invoke UserAlreadyExist exception within save method.");
        } catch (UserAlreadyExist e) {
            assertEquals("Entity with the same id(14567) is already exist", e.getMessage());
        }
    }
}
