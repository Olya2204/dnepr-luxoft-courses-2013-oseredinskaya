package com.luxoft.dnepr.courses.model;

public class Employee extends Entity {
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
