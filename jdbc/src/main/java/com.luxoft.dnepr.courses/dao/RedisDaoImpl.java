package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Redis;

public class RedisDaoImpl extends AbstractDao<Redis> {
    public RedisDaoImpl() {
        super(Redis.class);
    }
}
