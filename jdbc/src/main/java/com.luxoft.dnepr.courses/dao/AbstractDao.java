package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.exception.UserNotFound;
import com.luxoft.dnepr.courses.model.Employee;
import com.luxoft.dnepr.courses.model.Entity;
import com.luxoft.dnepr.courses.model.Redis;
import com.luxoft.dnepr.courses.storage.CreateDB;

import java.sql.*;

public class AbstractDao<T extends Entity> implements IDao<T> {
    private String tableName;
    private int secondParameter;

    public AbstractDao(Class<T> type) {
        if (type == Employee.class) {
            tableName = "employee";
        } else {
            tableName = "redis";
        }
    }

    private void setSecondParameter(T t) {
        if (t.getClass() == Employee.class) {
            Employee emp = (Employee) t;
            secondParameter = emp.getSalary();
        } else {
            Redis rd = (Redis) t;
            secondParameter = rd.getWeight();
        }
    }

    @Override
    public T save(T t) {
        setSecondParameter(t);
        try (Connection con = CreateDB.getConnection()) {
            Statement stmt = con.createStatement();
            if (t.getId() == 0) {
                t.setId(maxId() + 1);
            } else if (isIdExists(t.getId())) {
                throw new UserAlreadyExist("Entity with the same id(" + t.getId() + ") is already exist");
            }
            stmt.executeUpdate("INSERT INTO " + tableName + " VALUES ('" + t.getId() + "', '" + secondParameter + "'); ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    @Override
    public T update(T t) {
        if (!isIdExists(t.getId())) {
            throw new UserNotFound("Entity with the same id(" + t.getId() + ") is not found.");
        }
        setSecondParameter(t);
        try (Connection con = CreateDB.getConnection()) {
            Statement stmt = con.createStatement();
            if (t.getClass() == Employee.class) {
                stmt.executeUpdate("UPDATE " + tableName + " SET salary = " + secondParameter + " WHERE id =" + t.getId());
            } else {
                stmt.executeUpdate("UPDATE " + tableName + " SET weight = " + secondParameter + " WHERE id =" + t.getId());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    @Override
    public T get(int id) {
        try (Connection con = CreateDB.getConnection()) {
            Statement stmt = con.createStatement();
            if (isIdExists(id)) {
                if (tableName.equals("employee")) {
                    ResultSet rs = stmt.executeQuery("SELECT id, salary  FROM " + tableName + " WHERE id = " + id);
                    Employee emp = new Employee();
                    rs.next();
                    emp.setId(rs.getInt("id"));
                    emp.setSalary(rs.getInt("salary"));
                    return (T) emp;
                } else {
                    ResultSet rs = stmt.executeQuery("SELECT id, weight  FROM " + tableName + " WHERE id = " + id);
                    Redis rd = new Redis();
                    rs.next();
                    rd.setId(rs.getInt("id"));
                    rd.setWeight(rs.getInt("weight"));
                    return (T) rd;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        int result = 0;
        try (Connection con = CreateDB.getConnection()) {
            Statement stmt = con.createStatement();
            result = stmt.executeUpdate("DELETE FROM " + tableName + " WHERE id = " + id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (result > 0);
    }

    private int maxId() {
        int maxId = 0;
        try (Connection con = CreateDB.getConnection()) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT MAX(id) AS id FROM " + tableName);
            while (rs.next()) {
                maxId = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return maxId;
    }

    public boolean isIdExists(int id) {
        int result = 0;
        try (Connection con = CreateDB.getConnection()) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id FROM " + tableName + " WHERE id = " + id);
            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (result > 0);
    }

    public boolean isSecondParameterExists(int secondParameter) {
        int result = 0;
        try (Connection con = CreateDB.getConnection()) {
            Statement stmt = con.createStatement();
            ResultSet rs;
            if (tableName.equals("employee")) {
                rs = stmt.executeQuery("SELECT id FROM " + tableName + " WHERE salary = " + secondParameter);
            } else {
                rs = stmt.executeQuery("SELECT id FROM " + tableName + " WHERE weight = " + secondParameter);
            }
            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (result > 0);
    }
}
