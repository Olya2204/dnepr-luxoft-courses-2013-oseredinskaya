package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Entity;

public interface IDao<E extends Entity> {
    E save(E e);

    E update(E e);

    E get(int id);

    boolean delete(int id);
}
