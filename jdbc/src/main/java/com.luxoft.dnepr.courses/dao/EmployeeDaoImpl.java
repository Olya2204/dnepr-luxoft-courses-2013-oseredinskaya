package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Employee;

public class EmployeeDaoImpl extends AbstractDao<Employee> {
    public EmployeeDaoImpl() {
        super(Employee.class);
    }
}
