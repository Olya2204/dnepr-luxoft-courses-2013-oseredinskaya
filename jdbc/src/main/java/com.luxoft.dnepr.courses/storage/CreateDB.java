package com.luxoft.dnepr.courses.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDB {
    private static final String URL = "jdbc:h2:~/test";
    private static final String LOGIN = "root";
    private static final String PASS = "";

    public static void create() {
        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("DROP TABLE IF EXISTS employee");
            stmt.executeUpdate("DROP TABLE IF EXISTS redis");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS employee(id INTEGER PRIMARY KEY, salary INTEGER)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS redis(id INTEGER PRIMARY KEY, weight INTEGER)");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASS);
    }

    public static void main(String args[]) {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        create();
    }
}
