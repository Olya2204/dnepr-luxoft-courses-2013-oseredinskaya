<%@ page import="java.util.concurrent.atomic.AtomicInteger" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
    String role = (String) request.getSession().getAttribute("role");
    if (role == null || !role.equals("admin")) {
        response.sendRedirect("../user");
    }
%>
<html>
<head>
    <title>Session Data</title>
</head>
<body>
<b style="color: blue">Session Data:</b><br><br>

<table border="2">
    <tr style="color: blue" align="left" align="top">
        <td>Parameter</td>
        <td>Value</td>
    </tr>
    <tr align="left" align="top">
        <td>Active Sessions</td>
        <td><%= ((AtomicInteger) application.getAttribute("userSessions")).get() +
                ((AtomicInteger) application.getAttribute("adminSessions")).get() %>
        </td>
    </tr>
    <tr align="left" align="top">
        <td>Active Sessions (ROLE user)</td>
        <td><%=((AtomicInteger) application.getAttribute("userSessions")).get()%>
        </td>
    </tr>
    <tr align="left" align="top">
        <td>Active Sessions (ROLE admin)</td>
        <td><%=((AtomicInteger) application.getAttribute("adminSessions")).get()%>
        </td>
    </tr>
    <tr align="left" align="top">
        <td>Total Count of HttpRequests</td>
        <td><%=((AtomicInteger) application.getAttribute("allRequests")).get()%>
        </td>
    </tr>
    <tr align="left" align="top">
        <td>Total Count of POST HttpRequests</td>
        <td><%=((AtomicInteger) application.getAttribute("postRequests")).get()%>
        </td>
    </tr>
    <tr align="left" align="top">
        <td>Total Count of GET HttpRequests)</td>
        <td><%=((AtomicInteger) application.getAttribute("getRequests")).get()%>
        </td>
    </tr>
    <tr align="left" align="top">
        <td>Total Count of Other HttpRequests</td>
        <td>
            <%=((AtomicInteger) application.getAttribute("allRequests")).get() -
                    ((AtomicInteger) application.getAttribute("getRequests")).get() -
                    ((AtomicInteger) application.getAttribute("postRequests")).get()%>
        </td>
    </tr>
</table>
</body>
</html>


