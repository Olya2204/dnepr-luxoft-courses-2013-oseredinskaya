package com.luxoft.courses.javaWeb;

import com.luxoft.courses.javaWeb.entity.User;
import com.luxoft.courses.javaWeb.listener.MyServletListener;

import javax.servlet.http.HttpServlet;
import java.io.*;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;

public class LoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String servletPath = request.getServletPath();

        response.setContentType("text/html");
        if ("/login".equalsIgnoreCase(servletPath)) {

            String name = request.getParameter("name");
            String password = request.getParameter("password");
            String role = request.getParameter("role");
            User user = new User(name, password, role);

            List<User> listUsers = MyServletListener.getListUsers();

            if (listUsers.contains(user)) {
                HttpSession userSession = request.getSession(true);
                userSession.setAttribute("log", "true");
                userSession.setAttribute("name", name);
                userSession.setAttribute("role", role);
                response.sendRedirect("user");
            } else {
                request.setAttribute("error", true);
                request.getRequestDispatcher("").forward(request, response);
            }
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String servletPath = request.getServletPath();
        response.setContentType("text/html");

        if ("/logout".equalsIgnoreCase(servletPath)) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }
            response.sendRedirect("");
        }
    }
}