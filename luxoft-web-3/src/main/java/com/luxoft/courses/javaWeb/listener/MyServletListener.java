package com.luxoft.courses.javaWeb.listener;

import com.luxoft.courses.javaWeb.entity.User;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class MyServletListener implements ServletContextListener {
    private static List<User> listUsers = new CopyOnWriteArrayList<>();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();

        AtomicInteger userSessions = new AtomicInteger(0);
        AtomicInteger adminSessions = new AtomicInteger(0);
        AtomicInteger httpRequests = new AtomicInteger(0);
        AtomicInteger getRequests = new AtomicInteger(0);
        AtomicInteger postRequests = new AtomicInteger(0);

        sc.setAttribute("userSessions", userSessions);
        sc.setAttribute("adminSessions", adminSessions);
        sc.setAttribute("allRequests", httpRequests);
        sc.setAttribute("getRequests", getRequests);
        sc.setAttribute("postRequests", postRequests);

        String xml = sce.getServletContext().getInitParameter("users");
        InputStream is = sce.getServletContext().getResourceAsStream("META-INF/" + xml);
        try {
            DOMParser parser = new DOMParser();
            parser.parse(new InputSource(is));
            Document doc = parser.getDocument();
            NodeList users = doc.getElementsByTagName("user");

            for (int i = 0; i < users.getLength(); i++) {
                saveAttributes(users.item(i));
            }
        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private void saveAttributes(Node user) {
        NamedNodeMap attributes = user.getAttributes();
        String name = attributes.getNamedItem("name").getNodeValue();
        String password = attributes.getNamedItem("password").getNodeValue();
        String role = attributes.getNamedItem("role").getNodeValue();
        listUsers.add(new User(name, password, role));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();
        sc.removeAttribute("userSessions");
        sc.removeAttribute("adminSessions");
        sc.removeAttribute("allRequests");
        sc.removeAttribute("getRequests");
        sc.removeAttribute("postRequests");
    }

    public static List<User> getListUsers() {
        return listUsers;
    }
}
