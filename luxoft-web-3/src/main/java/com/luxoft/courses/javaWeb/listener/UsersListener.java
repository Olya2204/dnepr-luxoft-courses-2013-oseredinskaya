package com.luxoft.courses.javaWeb.listener;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicInteger;

public class UsersListener implements HttpSessionAttributeListener {

    @Override
    public void attributeAdded(HttpSessionBindingEvent hsbe) {
        HttpSession session = hsbe.getSession();
        String role = (String) session.getAttribute("role");
        ServletContext context = session.getServletContext();

        if (role.equals("user")) {
            ((AtomicInteger) context.getAttribute("userSessions")).incrementAndGet();
        } else {
            if (role.equals("admin")) {
                ((AtomicInteger) context.getAttribute("adminSessions")).incrementAndGet();
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent hsbe) {
        HttpSession session = hsbe.getSession();
        String role = (String) hsbe.getValue();
        ServletContext context = session.getServletContext();

        if (role.equals("user")) {
            ((AtomicInteger) context.getAttribute("userSessions")).decrementAndGet();
        } else {
            if (role.equals("admin")) {
                ((AtomicInteger) context.getAttribute("adminSessions")).decrementAndGet();
            }
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {

    }
}
