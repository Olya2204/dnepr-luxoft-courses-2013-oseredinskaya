package com.luxoft.courses.javaWeb.listener;


import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        ServletContext context = sre.getServletContext();
        ((AtomicInteger) context.getAttribute("allRequests")).incrementAndGet();

        if (((HttpServletRequest) sre.getServletRequest()).getMethod().equalsIgnoreCase("GET")) {
            ((AtomicInteger) context.getAttribute("getRequests")).incrementAndGet();
        }

        if (((HttpServletRequest) sre.getServletRequest()).getMethod().equalsIgnoreCase("POST")) {
            ((AtomicInteger) context.getAttribute("postRequests")).incrementAndGet();
        }
    }
}
