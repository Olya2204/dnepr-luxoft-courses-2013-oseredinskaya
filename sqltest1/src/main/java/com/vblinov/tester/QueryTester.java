package com.vblinov.tester;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Utility class, which loads SQL queries from resource files
 */
public final class QueryTester {

    private QueryTester() {

    }

    /**
     * Reads contents of resiurce file, which contains SQL query
     *
     * @param questionNumber number of question
     * @return contents of file in String representation
     */
    public static String readQueryNio(int questionNumber) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(QueryTester.class.getResource("/script/queries/q" + questionNumber + ".sql").toURI()));
            return StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded)).toString();
        } catch (Exception ex) {
            return null;
        }
    }

}
