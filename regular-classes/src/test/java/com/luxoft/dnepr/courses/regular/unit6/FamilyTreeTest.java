package com.luxoft.dnepr.courses.regular.unit6;


import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import com.sun.corba.se.spi.orbutil.fsm.Input;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;

public class FamilyTreeTest {

    @Test
    public void testIoUtils() {
        Person mark = new PersonImpl("Mark", Gender.MALE, "en", 32, null, null);
        Person sara = new PersonImpl("Sara", Gender.FEMALE, "en", 32, null, null);
        FamilyTree familyTreeSerialized = FamilyTreeImpl.create(new PersonImpl(null, Gender.MALE, "ua", 12, mark, sara));

        IOUtils.save("D:\\familyTree.json", familyTreeSerialized);

        FamilyTree familyTreeDeserialized = IOUtils.load("D:\\familyTree.json");
        Person root = familyTreeDeserialized.getRoot();
        Assert.assertEquals(null, root.getName());
        Assert.assertEquals(Gender.MALE, root.getGender());
        Assert.assertEquals("ua", root.getEthnicity());
        Assert.assertEquals(12, root.getAge());

        Person father = root.getFather();
        Assert.assertEquals("Mark", father.getName());
        Assert.assertEquals(Gender.MALE, father.getGender());
        Assert.assertEquals("en", father.getEthnicity());
        Assert.assertEquals(32, father.getAge());
        Assert.assertEquals(null, father.getFather());
        Assert.assertEquals(null, father.getMother());

        Person mother = root.getMother();
        Assert.assertEquals("Sara", mother.getName());
        Assert.assertEquals(Gender.FEMALE, mother.getGender());
        Assert.assertEquals("en", mother.getEthnicity());
        Assert.assertEquals(32, mother.getAge());
        Assert.assertEquals(null, mother.getFather());
        Assert.assertEquals(null, mother.getMother());
    }

    @Test
    public void testStream() throws IOException {
        PersonImpl person = new PersonImpl();
        person.setName("Mariya");
        person.setMother(new PersonImpl());
        FamilyTree familyTreeSerialized = FamilyTreeImpl.create(person);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        IOUtils.save(out, familyTreeSerialized);
        out.close();

        byte[] data = bos.toByteArray();
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        ObjectInputStream in = new ObjectInputStream(bis);

        FamilyTree familyTree = IOUtils.load(in);
       // Assert.assertEquals(familyTreeSerialized, familyTree);
    }
}
