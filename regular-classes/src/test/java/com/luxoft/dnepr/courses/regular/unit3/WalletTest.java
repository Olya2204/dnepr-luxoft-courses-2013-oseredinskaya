package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;


public class WalletTest {
    private static final String WALLET_BLOCKED_ERROR_MESSAGE = "Wallet is blocked";
    private static final String INSUFFICIENT_FOUNDS_ERROR_MESSAGE = "Amount to withdraw is more,than amount in the wallet";
    private static final String LIMIT_EXCEEDED_ERROR_MESSAGE = "Wallet limit is exceeded";

    private Wallet wallet = new Wallet();

    @Test
    public void testWithdraw() throws Exception {

        wallet.setId(3876489567811L);
        wallet.setAmount(new BigDecimal("12345.67"));
        wallet.setMaxAmount(new BigDecimal("15000.0"));
        wallet.setStatus(WalletStatus.ACTIVE);

        wallet.withdraw(new BigDecimal("1468.50"));

        BigDecimal amount = new BigDecimal("10877.17");

        assertEquals(amount.setScale(2).toString(), wallet.getAmount().setScale(2).toString());

        wallet.withdraw(new BigDecimal("13.34"));
        amount = new BigDecimal("10863.83");

        assertEquals(amount.setScale(2), wallet.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP));

    }

    @Test
    public void testTransfer() throws Exception {
        wallet.setId(3876489567811L);
        wallet.setAmount(new BigDecimal("12345.67"));
        assertEquals(new BigDecimal("12345.67"), wallet.getAmount());
        wallet.setMaxAmount(new BigDecimal("15000.00"));
        wallet.setStatus(WalletStatus.ACTIVE);

        wallet.transfer(new BigDecimal("1468.54"));
        BigDecimal amount = new BigDecimal("13814.21");

        assertEquals(amount.setScale(2), wallet.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
    }


    @Test
    public void testCheckTransfer() throws Exception {
        wallet.setId(3876489567811L);
        wallet.setAmount(new BigDecimal("12345.67"));
        wallet.setMaxAmount(new BigDecimal("15000.0"));
        wallet.setStatus(WalletStatus.ACTIVE);

        try {
            wallet.checkTransfer(new BigDecimal("14468.50"));
            Assert.fail("Method checkTransfer(BigDecimal amount) must invoke LimitExceededException.");
        } catch (LimitExceededException e) {
            assertEquals(LIMIT_EXCEEDED_ERROR_MESSAGE, e.getMessage());
        }

        wallet.setStatus(WalletStatus.BLOCKED);
        try {
            wallet.checkTransfer(new BigDecimal("14468.50"));
            Assert.fail("Method checkTransfer(BigDecimal amount) must invoke WalletIsBlockedException.");
        } catch (WalletIsBlockedException e) {
            assertEquals(WALLET_BLOCKED_ERROR_MESSAGE, e.getMessage());
        }

    }


    @Test
    public void testCheckWithdrawal() throws Exception {
        wallet.setId(3876489567811L);
        wallet.setAmount(new BigDecimal("12345.67"));
        wallet.setMaxAmount(new BigDecimal("15000.0"));
        wallet.setStatus(WalletStatus.ACTIVE);

        try {
            wallet.checkWithdrawal(new BigDecimal("14468.50"));
            Assert.fail("Method checkTransfer(BigDecimal amount) must invoke InsufficientWalletAmountException.");
        } catch (InsufficientWalletAmountException e) {
            assertEquals(INSUFFICIENT_FOUNDS_ERROR_MESSAGE, e.getMessage());
        }

        wallet.setStatus(WalletStatus.BLOCKED);
        try {
            wallet.checkWithdrawal(new BigDecimal("14468.50"));
            Assert.fail("Method checkTransfer(BigDecimal amount) must invoke WalletIsBlockedException.");
        } catch (WalletIsBlockedException e) {
            assertEquals(WALLET_BLOCKED_ERROR_MESSAGE, e.getMessage());
        }
    }
}
