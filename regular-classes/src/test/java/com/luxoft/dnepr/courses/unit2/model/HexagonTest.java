package com.luxoft.dnepr.courses.unit2.model;

import org.junit.Assert;
import org.junit.Test;

public class HexagonTest {
    private static final double DELTA = 0.000001;

    @Test
    public void testCalculateArea() {
        Assert.assertEquals(93.5307436, new Hexagon(6).calculateArea(), DELTA);
        Assert.assertEquals(116.6276411, new Hexagon(6.7).calculateArea(), DELTA);
    }

    @Test
    public void testCalculateAreaError() {
        try {
            new Hexagon(-6);
            Assert.fail("-6 must invoke IllegalArgumentException within Hexagon(double side) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Input value '-6.0' is less than 0 or equals to zero", e.getMessage());
        }


        try {
            new Hexagon(0);
            Assert.fail("0 must invoke IllegalArgumentException within Hexagon(double side) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Input value '0.0' is less than 0 or equals to zero", e.getMessage());
        }

    }
}
