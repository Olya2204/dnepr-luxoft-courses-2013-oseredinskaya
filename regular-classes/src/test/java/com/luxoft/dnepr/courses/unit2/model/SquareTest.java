package com.luxoft.dnepr.courses.unit2.model;

import org.junit.Assert;
import org.junit.Test;

public class SquareTest {
    private static final double DELTA = 0.000001;

    @Test
    public void testCalculateArea() {
        Assert.assertEquals(16.0, new Square(4).calculateArea(), DELTA);
        Assert.assertEquals(30.25, new Square(5.5).calculateArea(), DELTA);
    }

    @Test
    public void testCalculateAreaError() {
        try {
            new Square(-6);
            Assert.fail("-6 must invoke IllegalArgumentException within Square(double side) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Input value '-6.0' is less than 0 or equals to zero", e.getMessage());
        }


        try {
            new Square(0);
            Assert.fail("0 must invoke IllegalArgumentException within Square(double side) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Input value '0.0' is less than 0 or equals to zero", e.getMessage());
        }

    }

}
