package com.luxoft.dnepr.courses.unit2.model;

import org.junit.Assert;
import org.junit.Test;

public class CircleTest {
    private static final double DELTA = 0.000001;


    @Test
    public void testCalculateArea() {
        Assert.assertEquals(78.539816, new Circle(5).calculateArea(), DELTA);
        Assert.assertEquals(58.0880481, new Circle(4.3).calculateArea(), DELTA);
    }

    @Test
    public void testCalculateAreaError() {
        try {
            new Circle(-5);
            Assert.fail("-5 must invoke IllegalArgumentException within Circle(double radius) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Input value '-5.0' is less than 0 or equals to zero", e.getMessage());
        }

        try {
            new Circle(0);
            Assert.fail("0 must invoke IllegalArgumentException within Circle(double radius) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Input value '0.0' is less than 0 or equals to zero", e.getMessage());
        }

    }

}
