package com.luxoft.dnepr.courses.regular.unit4;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EqualSetTest {
    private static final String NULL_VALUE_ERROR_MESSAGE = "Input parameter is null";

    @Test
    public void testCreateEqualSet() throws Exception {
        Set<String> mySet = new EqualSet<String>();
        assertTrue(mySet.isEmpty());

        assertTrue(mySet.add("cat"));
        assertTrue(mySet.add("dog"));
        assertTrue(mySet.add("chicken"));
        assertTrue(mySet.add(null));
        assertFalse(mySet.add("cat"));
        assertFalse(mySet.add(null));
        assertEquals(4, mySet.size());


        List<Integer> listInteger = new ArrayList<Integer>();
        for (int i = 0; i < 130; i = i + 2) {
            listInteger.add(i);
        }

        Set<Integer> setInteger = new EqualSet<Integer>(listInteger);
        listInteger.add(0);
        listInteger.add(2);
        Set<Integer> otherSetInteger = new EqualSet<Integer>(listInteger);
        assertEquals(setInteger.size(), otherSetInteger.size());

        listInteger.add(9);
        listInteger.add(7);
        assertTrue(setInteger.addAll(listInteger));
        assertEquals(67, setInteger.size());
    }

    @Test
    public void testSetOperations() throws Exception {
        Set<String> mySet = new EqualSet<String>();
        assertTrue(mySet.isEmpty());

        assertTrue(mySet.add("cat"));
        assertTrue(mySet.add("dog"));
        assertTrue(mySet.add("chicken"));
        assertTrue(mySet.add(null));

        assertTrue(mySet.contains("cat"));
        mySet.remove("cat");
        assertFalse(mySet.contains("cat"));

        List<String> newListString = new ArrayList<String>();
        newListString.add("dog");
        assertTrue(mySet.retainAll(newListString));
        assertEquals(1, mySet.size());

        List<Integer> listInteger = new ArrayList<Integer>();
        for (int i = 0; i < 130; i = i + 2) {
            listInteger.add(i);
        }

        Set<Integer> setInteger = new EqualSet<Integer>(listInteger);

        assertTrue(setInteger.containsAll(listInteger));
        setInteger.add(9);
        setInteger.add(7);

        setInteger.removeAll(listInteger);
        assertEquals(2, setInteger.size());

        setInteger.clear();
        assertTrue(setInteger.isEmpty());

    }

    @Test
    public void testSetToArray() throws Exception {
        Set<String> mySet = new EqualSet<String>();

        mySet.add("cat");
        mySet.add("dog");

        Object[] masObjects = mySet.toArray();
        assertEquals(2, masObjects.length);

        String[] masStrings = mySet.toArray(new String[mySet.size()]);
        assertEquals(2, masStrings.length);

    }

    @Test
    public void testSetIterator() throws Exception {
        Set<String> mySet = new EqualSet<String>();

        mySet.add("cat");
        mySet.add("dog");
        mySet.add("chicken");

        Iterator iter = mySet.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }

    @Test
    public void testInputNullValue() throws Exception {
        Set<String> setStrings = new EqualSet<String>();
        try {
            setStrings.addAll(null);
            Assert.fail("null must invoke IllegalArgumentException within addAll method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(NULL_VALUE_ERROR_MESSAGE, e.getMessage());
        }
        try {
            setStrings.containsAll(null);
            Assert.fail("null must invoke IllegalArgumentException within containsAll method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(NULL_VALUE_ERROR_MESSAGE, e.getMessage());
        }
        try {
            setStrings.retainAll(null);
            Assert.fail("null must invoke IllegalArgumentException within retainAll method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(NULL_VALUE_ERROR_MESSAGE, e.getMessage());
        }
        try {
            setStrings.removeAll(null);
            Assert.fail("null must invoke IllegalArgumentException within removeAll method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(NULL_VALUE_ERROR_MESSAGE, e.getMessage());
        }
        try {
            setStrings.toArray(null);
            Assert.fail("null must invoke IllegalArgumentException within toArray(T[] a) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(NULL_VALUE_ERROR_MESSAGE, e.getMessage());
        }
        try {
            new EqualSet<String>(null);
            Assert.fail("null must invoke IllegalArgumentException within EqualSet(Collection<? extends E> collection) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(NULL_VALUE_ERROR_MESSAGE, e.getMessage());
        }
    }

}
