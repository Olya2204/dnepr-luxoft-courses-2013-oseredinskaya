package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;
import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class BankTest {

    private static final String JAVA_VERSION_ERROR_MESSAGE = "Java version on your machine is different from the expected java version";
    private static final String CURRENT_JAVA_VERSION = System.getProperty("java.version");

    private Bank bank = new Bank(CURRENT_JAVA_VERSION);

    @Before
    public void setUpBankUsers() {
        User firstUser = new User(345778999231L, "Anton", new Wallet(4667822111L, new BigDecimal("12345.67"), WalletStatus.ACTIVE, new BigDecimal("15000.0")));
        User secondUser = new User(34433990031L, "Yuriy", new Wallet(5890001245L, new BigDecimal("15145.17"), WalletStatus.ACTIVE, new BigDecimal("20000.0")));

        Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();
        users.put(firstUser.getId(), firstUser);
        users.put(secondUser.getId(), secondUser);
        bank.setUsers(users);
    }

    @Test
    public void testCreateBank() throws Exception {
        Bank bank = new Bank(CURRENT_JAVA_VERSION);
        try {
            Bank otherBank = new Bank("1.5.0_15");
            Assert.fail("Method Bank must invoke IllegalJavaVersionError.");
        } catch (IllegalJavaVersionError e) {
            assertEquals(JAVA_VERSION_ERROR_MESSAGE, e.getMessage());
        }
    }


    @Test
    public void testMakeMoneyTransaction() throws Exception {
        Long firstUserId = 345778999231L;
        Long secondUserId = 34433990031L;
        bank.makeMoneyTransaction(firstUserId, secondUserId, new BigDecimal("1789.67"));
        BigDecimal firstUserAmount = bank.getUsers().get(firstUserId).getWallet().getAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
        assertEquals(new BigDecimal("10556.00").setScale(2), firstUserAmount);

        BigDecimal secondUserAmount = bank.getUsers().get(secondUserId).getWallet().getAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
        assertEquals(new BigDecimal("16934.84").setScale(2), secondUserAmount);

    }

    @Test
    public void testCheckId() throws Exception {
        Long firstUserId = 345778999231L;
        Long secondUserId = 34433990031L;
        try {
            bank.makeMoneyTransaction(11111111111L, secondUserId, new BigDecimal("1789.67"));
            Assert.fail("Method makeMoneyTransaction must invoke NoUserFoundException.");
        } catch (NoUserFoundException e) {
            assertEquals("User with this id '11111111111' is not found", e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(firstUserId, 22222222L, new BigDecimal("1789.67"));
            Assert.fail("Method makeMoneyTransaction must invoke NoUserFoundException.");
        } catch (NoUserFoundException e) {
            assertEquals("User with this id '22222222' is not found", e.getMessage());
        }


    }

    @Test
    public void testWalletStatusError() throws Exception {
        Long firstUserId = 345778999231L;
        Long secondUserId = 34433990031L;

        bank.getUsers().get(firstUserId).getWallet().setStatus(WalletStatus.BLOCKED);
        bank.getUsers().get(secondUserId).getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            bank.makeMoneyTransaction(firstUserId, secondUserId, new BigDecimal("1789.67"));
            Assert.fail("Method makeMoneyTransaction must invoke TransactionException.");
        } catch (TransactionException e) {
            assertEquals("User 'Anton' wallet is blocked", e.getMessage());
        }

        bank.getUsers().get(firstUserId).getWallet().setStatus(WalletStatus.ACTIVE);

        try {
            bank.makeMoneyTransaction(firstUserId, secondUserId, new BigDecimal("1789.67"));
            Assert.fail("Method makeMoneyTransaction must invoke TransactionException.");
        } catch (TransactionException e) {
            assertEquals("User 'Yuriy' wallet is blocked", e.getMessage());
        }
    }

    @Test
    public void testSenderAndReceiverAccountError() throws Exception {
        Long firstUserId = 345778999231L;
        Long secondUserId = 34433990031L;
        try {
            bank.makeMoneyTransaction(firstUserId, secondUserId, new BigDecimal("13000.6778"));
            Assert.fail("Method makeMoneyTransaction must invoke TransactionException.");
        } catch (TransactionException e) {
            assertEquals("User 'Anton' has insufficient funds (12345.67 < 13000.68)", e.getMessage());
        }

        bank.getUsers().get(secondUserId).getWallet().setMaxAmount(new BigDecimal("16000.0"));
        try {
            bank.makeMoneyTransaction(firstUserId, secondUserId, new BigDecimal("1300.6778"));
            Assert.fail("Method makeMoneyTransaction must invoke TransactionException.");
        } catch (TransactionException e) {
            assertEquals("User 'Yuriy' wallet limit exceeded (15145.17 + 1300.68 > 16000.00)", e.getMessage());
        }

    }
}
