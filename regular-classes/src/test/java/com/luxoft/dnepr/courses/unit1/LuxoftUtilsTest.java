package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {

    @Test
    public void testGetMonthName() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        Assert.assertEquals("Декабрь", LuxoftUtils.getMonthName(12, "ru"));
        Assert.assertEquals("April", LuxoftUtils.getMonthName(4, "en"));
    }

    @Test
    public void testGetMonthNameError() {
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(5, "hindi"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(13, "hindi"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(0, "ru"));
        Assert.assertEquals("Unknown month", LuxoftUtils.getMonthName(-5, "en"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(10, null));
    }


    @Test
    public void testBinaryToDecimal() {
        Assert.assertEquals("91", LuxoftUtils.binaryToDecimal("1011011"));
        Assert.assertEquals("247", LuxoftUtils.binaryToDecimal("11110111"));
    }

    @Test
    public void testBinaryToDecimalError() {
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("test"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("10s1"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(null));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(""));
    }


    @Test
    public void testDecimalToBinary() {
        Assert.assertEquals("1011011", LuxoftUtils.decimalToBinary("91"));
        Assert.assertEquals("11110111", LuxoftUtils.decimalToBinary("247"));
    }

    @Test
    public void testDecimalToBinaryError() {
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("test"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("35s4"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(null));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(""));
    }

    @Test
    public void testSortArray() {
        Assert.assertArrayEquals(new int[]{1, 3, 5, 7, 9}, LuxoftUtils.sortArray(new int[]{7, 3, 5, 9, 1}, true));
        Assert.assertArrayEquals(new int[]{9, 7, 5, 3, 1}, LuxoftUtils.sortArray(new int[]{7, 3, 5, 9, 1}, false));
    }

    @Test
    public void testSortArrayError() {
        try {
            LuxoftUtils.sortArray(null, true);
            Assert.fail("null must invoke NullPointerException within LuxoftUtils.sortArray(int[] array, boolean asc) method.");
        } catch (NullPointerException e) {
            Assert.assertEquals("Null value", e.getMessage());
        }
    }

}
