package com.luxoft.dnepr.courses.regular.unit7;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class _WordStorageTest {
    private WordStorage wordStorage;

    @Before
    public void setUp() {
        wordStorage = new WordStorage();
    }

    @After
    public void tearDown() {
        wordStorage = null;
    }

    @Test
    public void testSave() throws Exception {
        List<Thread> treads = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            Thread tread = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 10000; j++) {
                        wordStorage.save(Integer.toString(j));
                    }
                }
            });
            treads.add(tread);
            tread.start();
        }

        for (Thread thread : treads) {
            thread.join();
        }
        assertEquals(10000, wordStorage.getWordStatistics().size());
        for (Number number : wordStorage.getWordStatistics().values()) {
            assertEquals(50, number.intValue());
        }

    }

    @Test
    public void testGetWordStatistics() {
        assertEquals(0, wordStorage.getWordStatistics().size());
        wordStorage.save("test");
        assertEquals(1, wordStorage.getWordStatistics().size());

        try {
            wordStorage.getWordStatistics().remove("test");
            fail("wordStatistics isn't unmodifiable");
        } catch (UnsupportedOperationException e) {
        }
    }

}
