package com.luxoft.dnepr.courses.regular.unit5;


import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class DaoTest {

    @BeforeClass
    public static void setUpStorage() {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        Employee employee = new Employee();
        employee.setId(14566L);
        employee.setSalary(1000);

        Employee employeeWithNullId = new Employee();
        employeeWithNullId.setId(null);
        employeeWithNullId.setSalary(2000);

        IDao<Redis> redisDao = new RedisDaoImpl();
        Redis redis = new Redis();
        redis.setId(16780L);
        redis.setWeight(5);

        Redis otherRedis = new Redis();
        otherRedis.setId(12121L);
        otherRedis.setWeight(6);

        employeeDao.save(employeeWithNullId);
        employeeDao.save(employee);
        redisDao.save(redis);
        redisDao.save(otherRedis);
    }

    @Test
    public void testSave() throws Exception {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        assertTrue(EntityStorage.getEntities().containsKey(1L));

        Employee otherEmployee = new Employee();
        otherEmployee.setId(null);
        otherEmployee.setSalary(2000);

        employeeDao.save(otherEmployee);

        assertTrue(EntityStorage.getEntities().containsKey(16781L));

        otherEmployee.setId(14566L);
        try {
            employeeDao.save(otherEmployee);
            Assert.fail("This id must invoke UserAlreadyExist exception within save method.");
        } catch (UserAlreadyExist e) {
            assertEquals("Entity with the same id(14566) is already exist", e.getMessage());
        }


    }

    @Test
    public void testUpdate() throws Exception {
        IDao<Redis> redisDao = new RedisDaoImpl();

        Redis otherRedis = new Redis();
        otherRedis.setId(null);
        otherRedis.setWeight(7);

        try {
            redisDao.update(otherRedis);
            Assert.fail("This id must invoke UserNotFound exception within update method.");
        } catch (UserNotFound e) {
            assertEquals("Entity with the same id(null) is not found.", e.getMessage());
        }

        otherRedis.setId(11111L);
        try {
            redisDao.update(otherRedis);
            Assert.fail("This id must invoke UserNotFound exception within update method.");
        } catch (UserNotFound e) {
            assertEquals("Entity with the same id(11111) is not found.", e.getMessage());
        }

        otherRedis.setId(14566L);
        redisDao.update(otherRedis);
    }

    @Test
    public void testGet() throws Exception {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        IDao<Redis> redisDao = new RedisDaoImpl();

        assertEquals(null, employeeDao.get(55555L));
        Employee employee = employeeDao.get(14566L);
        Redis redis = redisDao.get(16780L);

        assertEquals((Object) 14566L, employee.getId());
        assertEquals((Object) 16780L, redis.getId());

        assertEquals(null, employeeDao.get(16780L));
    }

    @Test
    public void testDelete() throws Exception {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        IDao<Redis> redisDao = new RedisDaoImpl();

        assertFalse(employeeDao.delete(16780L));
        assertFalse(employeeDao.delete(90L));
        assertTrue(redisDao.delete(12121L));
    }

}
