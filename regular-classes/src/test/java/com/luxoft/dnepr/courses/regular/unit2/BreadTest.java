package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.*;

public class BreadTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Bread bread = productFactory.createBread("code", "Bread", 10, 1.0);
        Bread cloned = (Bread) bread.clone();
        cloned.setName("My bread");
        cloned.setPrice(12);

        assertNotSame(bread, cloned);
        assertEquals("Bread", bread.getName());
        assertEquals(10, bread.getPrice(), 0);
        assertEquals("My bread", cloned.getName());
        assertEquals(12, cloned.getPrice(), 0);
    }

    @Test
    public void testEquals() throws Exception {
        Bread bread = productFactory.createBread("code", "Bread", 10, 1.0);
        Bread myBread = productFactory.createBread("code", "My Bread", 12, 1.0);
        Bread anotherBread = productFactory.createBread("code", "Bread", 12, 1.0);

        assertTrue(bread.equals(anotherBread));
        assertFalse(myBread.equals(bread));

    }
}
