package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LuxoftUtilsTest {

    List<Figure> figures = new ArrayList<Figure>();

    @Before
    public void setup() {
        figures.add(new Circle(5));
        figures.add(new Hexagon(2));
        figures.add(new Square(4));
    }

    @Test
    public void testSortArray() {
        Assert.assertArrayEquals(new String[]{"Avc", "Fdr", "avc", "fdr", "fer"},
                LuxoftUtils.sortArray(new String[]{"fdr", "avc", "Avc", "Fdr", "fer"}, true));
        Assert.assertArrayEquals(new String[]{"fer", "fdr", "avc", "Fdr", "Avc"},
                LuxoftUtils.sortArray(new String[]{"fdr", "avc", "Avc", "Fdr", "fer"}, false));
    }

    @Test
    public void testSortArrayError() {
        try {
            LuxoftUtils.sortArray(null, true);
            Assert.fail("null must invoke NullPointerException within LuxoftUtils.sortArray(String[] array, boolean asc) method.");
        } catch (NullPointerException e) {
            Assert.assertEquals("Null value", e.getMessage());
        }
    }

    @Test
    public void testWordAverageLength() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), 0.01);
    }

    @Test
    public void testWordAverageLengthError() {
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(""), 0.1);
        try {
            LuxoftUtils.wordAverageLength(null);
            Assert.fail("null must invoke NullPointerException within LuxoftUtils.wordAverageLength(String str) method.");
        } catch (NullPointerException e) {
            Assert.assertEquals("Null value", e.getMessage());
        }
    }

    @Test
    public void testReverseWords() {
        Assert.assertEquals("cba trf rgrg ", LuxoftUtils.reverseWords("abc frt grgr "));
    }

    @Test
    public void testReverseWordsError() {
        Assert.assertEquals("Empty string", LuxoftUtils.reverseWords(""));
        try {
            LuxoftUtils.reverseWords(null);
            Assert.fail("null must invoke NullPointerException within LuxoftUtils.reverseWords(String str) method.");
        } catch (NullPointerException e) {
            Assert.assertEquals("Null value", e.getMessage());
        }
    }

    @Test
    public void testCalculateOverallArea() {
        Assert.assertEquals(104.932121, LuxoftUtils.calculateOverallArea(figures), 0.000001);
    }

    @Test
    public void testCalculateOverallAreaError() {
        try {
            LuxoftUtils.calculateOverallArea(null);
            Assert.fail("null must invoke NullPointerException within calculateOverallArea(List<Figure> figures) method.");
        } catch (NullPointerException e) {
            Assert.assertEquals("Null value", e.getMessage());
        }

    }


    @Test
    public void testGetCharEntries() {
        Assert.assertArrayEquals(new char[]{'s', 'A', 'B', 'C', 'S', 'a', 'b', 'c'}, LuxoftUtils.getCharEntries("aAbBcCssS"));

    }


    @Test
    public void testGetCharEntriesError() {

        try {
            LuxoftUtils.getCharEntries("");
            Assert.fail("Empty string must invoke IllegalArgumentException within LuxoftUtils.getCharEntries(String str) method.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Empty string", e.getMessage());
        }
        try {
            LuxoftUtils.getCharEntries(null);
            Assert.fail("null must invoke NullPointerException within LuxoftUtils.getCharEntries(String str) method.");
        } catch (NullPointerException e) {
            Assert.assertEquals("Null value", e.getMessage());
        }
    }

}
