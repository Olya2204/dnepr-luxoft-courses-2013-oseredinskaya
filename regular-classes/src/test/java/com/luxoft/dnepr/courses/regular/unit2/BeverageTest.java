package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.*;

public class BeverageTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Beverage beverage = productFactory.createBeverage("code", "Cola", 10, true);
        Beverage cloned = (Beverage) beverage.clone();
        cloned.setName("Sprite");
        cloned.setPrice(12);

        assertNotSame(beverage, cloned);
        assertEquals("Cola", beverage.getName());
        assertEquals(10, beverage.getPrice(), 0);
        assertEquals("Sprite", cloned.getName());
        assertEquals(12, cloned.getPrice(), 0);
    }

    @Test
    public void testEquals() throws Exception {
        Beverage beverage = productFactory.createBeverage("code", "Cola", 10, true);
        Beverage myBeverage = productFactory.createBeverage("code", "Wine", 100, false);
        Beverage anotherBeverage = productFactory.createBeverage("code", "Cola", 15, true);

        assertTrue(beverage.equals(anotherBeverage));
        assertFalse(myBeverage.equals(beverage));

    }
}
