package com.luxoft.dnepr.courses.unit2.model;

import org.junit.Assert;
import org.junit.Test;

public class FigureTest {
    private static final double DELTA = 0.000001;

    @Test
    public void testCalculateArea() {
        Assert.assertEquals(93.5307436, new Hexagon(6).calculateArea(), DELTA);
        Assert.assertEquals(58.0880481, new Circle(4.3).calculateArea(), DELTA);
        Assert.assertEquals(30.25, new Square(5.5).calculateArea(), DELTA);
    }


}
