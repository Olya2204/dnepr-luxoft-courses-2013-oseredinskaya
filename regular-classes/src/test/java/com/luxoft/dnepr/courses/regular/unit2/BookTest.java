package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        cloned.setName("Book");
        cloned.setPrice(100);
        cloned.setPublicationDate(new GregorianCalendar(2010, 0, 1).getTime());

        Date dataBook=  new GregorianCalendar(2006, 0, 1).getTime();
        Date dataCloned=  new GregorianCalendar(2010, 0, 1).getTime();

        assertNotSame(book, cloned);

        assertEquals("Thinking in Java", book.getName());
        assertEquals(200, book.getPrice(), 0);
        assertEquals(dataBook, book.getPublicationDate());

        assertEquals("Book", cloned.getName());
        assertEquals(100, cloned.getPrice(), 0);
        assertEquals(dataCloned, cloned.getPublicationDate());


    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book myBook = productFactory.createBook("code", "Java", 190, new GregorianCalendar(2006, 0, 1).getTime());
        Book anotherBook = productFactory.createBook("code", "Thinking in Java", 190, new GregorianCalendar(2006, 0, 1).getTime());

        assertTrue(book.equals(anotherBook));
        assertFalse(myBook.equals(book));
    }
}
