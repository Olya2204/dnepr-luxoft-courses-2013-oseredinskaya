package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class TransactionException extends Exception {

    public TransactionException(String message) {
        super(message);
    }

}
