package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents word statistics storage.
 */
public class WordStorage {
    private final Map<String, Integer> wordStorage = new HashMap<>();

    /**
     * Saves given word and increments count of occurrences.
     *
     * @param word
     */
    public synchronized void save(String word) {
        if (wordStorage.containsKey(word)) {
            wordStorage.put(word, wordStorage.get(word) + 1);
        } else {
            wordStorage.put(word, 1);
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(wordStorage);
    }
}
