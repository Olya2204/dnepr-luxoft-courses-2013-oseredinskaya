package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.StringTokenizer;

public class PersonImpl implements Person {
    private String name;
    private Gender gender;
    private String ethnicity;
    private int age;
    private Person father;
    private Person mother;

    public PersonImpl() {
    }

    public PersonImpl(String name, Gender gender, String ethnicity, int age, Person father, Person mother) {
        this.name = name;
        this.gender = gender;
        this.ethnicity = ethnicity;
        this.age = age;
        this.father = father;
        this.mother = mother;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEthnicity() {
        return ethnicity;
    }

    @Override
    public Person getFather() {
        return father;
    }

    @Override
    public Person getMother() {
        return mother;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeUTF(this.toString());
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Person person = readPerson(new StringTokenizer(in.readUTF()));
        this.name = person.getName();
        this.ethnicity = person.getEthnicity();
        this.father = person.getFather();
        this.mother = person.getMother();
        this.gender = person.getGender();
        this.age = person.getAge();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if (name != null) sb.append("\"name\":\"").append(name).append("\",");
        if (gender != null) sb.append("\"gender\":\"").append(getGender().toString()).append("\",");
        if (ethnicity != null) sb.append("\"ethnicity\":\"").append(ethnicity).append("\",");
        if (age != 0) sb.append("\"age\":\"").append(age).append("\"");
        if (father != null) {
            sb.append(",\"father\":");
            sb.append(father.toString());
        }
        if (mother != null) {
            sb.append(",\"mother\":");
            sb.append(mother.toString());
        }
        sb.append("}");
        return sb.toString();
    }

    public static PersonImpl readPerson(StringTokenizer st) {
        PersonImpl root = new PersonImpl();
        while (st.hasMoreTokens()) {
            String token = st.nextToken(":\",");
            switch (token) {
                case "name": {
                    root.setName(st.nextToken());
                    break;
                }
                case "ethnicity": {
                    root.setEthnicity(st.nextToken());
                    break;
                }
                case "father": {
                    root.setFather(readPerson(st));
                    break;
                }
                case "mother": {
                    root.setMother(readPerson(st));
                    break;
                }
                case "gender": {
                    root.setGender(Gender.valueOf(st.nextToken()));
                    break;
                }
                case "age": {
                    root.setAge(Integer.parseInt(st.nextToken()));
                    break;
                }
                default: {
                    if (token.endsWith("}")) {
                        return root;
                    }
                }
            }
        }
        return root;
    }
}
