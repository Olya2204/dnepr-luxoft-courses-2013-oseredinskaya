package com.luxoft.dnepr.courses.regular.unit7;


import java.io.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private final BlockingQueue<File> sharedQueue;
    private final WordStorage wordStorage;
    private final List<File> processedFiles;

    public Consumer(BlockingQueue<File> sharedQueue, WordStorage wordStorage, List<File> processedFiles) {
        this.sharedQueue = sharedQueue;
        this.wordStorage = wordStorage;
        this.processedFiles = processedFiles;
    }

    @Override
    public void run() {
        try {
            boolean done = false;
            while (!done) {
                File file = sharedQueue.take();
                if (file == Producer.STOP) {
                    sharedQueue.put(file);
                    done = true;
                } else {
                    processedFiles.add(file);
                    readFile(file);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void readFile(File file) {


        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringBuffer fileContents = new StringBuffer();
        try {
            String myString;
            while ((myString = br != null ? br.readLine() : null) != null) {
                fileContents.append(myString);
                fileContents.append("\n");
            }
            saveToStorage(fileContents.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveToStorage(String fileContents) {
        String[] words = fileContents.split("(?U)[\\W_]+");
        if (words == null) {
            return;
        }
        for (String word : words) {
            if (!word.equals("")) {
                wordStorage.save(word);
            }
        }
    }
}
