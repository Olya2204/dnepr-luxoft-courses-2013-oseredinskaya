package com.luxoft.dnepr.courses.unit2.model;

public class Square extends Figure {
    double side;

    public Square(double side) {
        if (side <= 0)
            throw new IllegalArgumentException(String.format("Input value '%s' is less than 0 or equals to zero", side));
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return Math.pow(this.side, 2);
    }
}
