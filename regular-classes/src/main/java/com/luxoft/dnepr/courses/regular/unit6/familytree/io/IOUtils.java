package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import java.io.*;
import java.util.StringTokenizer;

public class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(String filename) {
        FamilyTree familyTree = null;
        try {
            familyTree = load(new FileInputStream(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return familyTree;
    }

    public static FamilyTree load(InputStream is) {
        FamilyTree familyTree = null;
        try {
            ObjectInputStream in = new ObjectInputStream(is);
            familyTree = (FamilyTree) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return familyTree;
    }

    public static void save(String filename, FamilyTree familyTree) {
        try {
            save(new FileOutputStream(filename), familyTree);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void save(OutputStream os, FamilyTree familyTree) {
        try (ObjectOutputStream output = new ObjectOutputStream(os)) {
            output.writeObject(familyTree);
            output.flush();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
