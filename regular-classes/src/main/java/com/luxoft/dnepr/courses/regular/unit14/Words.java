package com.luxoft.dnepr.courses.regular.unit14;

import java.util.ArrayList;
import java.util.List;

public class Words {

    private static List<String> knownWords = new ArrayList<>();
    private static List<String> unknownWords = new ArrayList<>();

    public static void estimatedVocabulary(int allWords) {
        int countWords = allWords * (knownWords.size() + 1) / (knownWords.size() + unknownWords.size() + 1);
        System.out.println("Your estimated vocabulary is " + countWords + " words.");
    }

    public static void addToMyVocabulary(String word, boolean isKnown) {
        if (isKnown) {
            knownWords.add(word);
        } else {
            unknownWords.add(word);
        }
    }

}
