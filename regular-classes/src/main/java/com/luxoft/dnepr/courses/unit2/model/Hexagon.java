package com.luxoft.dnepr.courses.unit2.model;


public class Hexagon extends Figure {
    double side;

    public Hexagon(double side) {
        if (side <= 0)
            throw new IllegalArgumentException(String.format("Input value '%s' is less than 0 or equals to zero", side));
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return (3 * Math.sqrt(3) * Math.pow(this.side, 2)) / 2;
    }
}
