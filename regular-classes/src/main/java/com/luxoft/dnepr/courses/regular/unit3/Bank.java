package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankInterface {

    private static final String JAVA_VERSION_ERROR_MESSAGE = "Java version on your machine is different from the expected java version";
    private static final String CURRENT_JAVA_VERSION = System.getProperty("java.version");

    private Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();

    Bank(String expectedJavaVersion) {
        if (!CURRENT_JAVA_VERSION.equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(CURRENT_JAVA_VERSION, expectedJavaVersion,
                    JAVA_VERSION_ERROR_MESSAGE);
        }
    }

    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        checkId(fromUserId);
        checkId(toUserId);

        checkWalletStatus(fromUserId);
        checkWalletStatus(toUserId);

        checkSenderAccount(fromUserId, amount);
        checkReceiverAccount(toUserId, amount);

        transfer(fromUserId, toUserId, amount);
    }

    private void checkId(Long userId) throws NoUserFoundException {
        if (!users.containsKey(userId)) {
            throw new NoUserFoundException(userId, "User with this id '" + userId.toString() + "' is not found");
        }
    }

    private void checkWalletStatus(Long userId) throws TransactionException {
        User user = (User) users.get(userId);
        if (user.getWallet().getStatus().equals(WalletStatus.BLOCKED)) {
            throw new TransactionException("User '" + user.getName() + "' wallet is blocked");
        }
    }

    private void checkSenderAccount(Long fromUserId, BigDecimal amount) throws TransactionException {
        User user = (User) users.get(fromUserId);
        String userWalletAmount = user.getWallet().getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        try {
            users.get(fromUserId).getWallet().checkWithdrawal(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '" + user.getName() + "' wallet is blocked");
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException("User '" + user.getName() + "' has insufficient funds ("
                    + userWalletAmount + " < " + amount.setScale(2, BigDecimal.ROUND_HALF_UP).toString() + ")");
        }
    }

    private void checkReceiverAccount(Long toUserId, BigDecimal amount) throws TransactionException {
        User user = (User) users.get(toUserId);
        String userWalletAmount = users.get(toUserId).getWallet().getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        String userWalletMaxAmount = users.get(toUserId).getWallet().getMaxAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        try {
            users.get(toUserId).getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '" + user.getName() + "' wallet is blocked");
        } catch (LimitExceededException e) {
            throw new TransactionException("User '" + user.getName() + "' wallet limit exceeded ("
                    + userWalletAmount + " + " + amount.setScale(2, BigDecimal.ROUND_HALF_UP).toString() + " > " + userWalletMaxAmount + ")");
        }
    }

    private void transfer(Long fromUserId, Long toUserId, BigDecimal amount) {
        users.get(fromUserId).getWallet().withdraw(amount);
        users.get(toUserId).getWallet().transfer(amount);
    }
}
