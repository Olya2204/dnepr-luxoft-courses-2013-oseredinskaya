package com.luxoft.dnepr.courses.regular.unit5.exception;


public class UserNotFound extends RuntimeException {

    public UserNotFound() {

    }

    public UserNotFound(String message) {
        super(message);
    }
}
