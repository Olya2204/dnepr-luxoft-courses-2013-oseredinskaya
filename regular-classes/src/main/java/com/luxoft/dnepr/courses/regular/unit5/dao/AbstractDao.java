package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;
import java.util.TreeMap;

public class AbstractDao<T extends Entity> implements IDao<T> {
    private Class<T> type;

    public AbstractDao(Class<T> type) {
        this.type = type;
    }

    @Override
    public T save(T t) {
        if (t.getId() == null) {
            t.setId(maxId() + 1);
        } else if (entityMap().containsKey(t.getId())) {
            throw new UserAlreadyExist("Entity with the same id(" + t.getId() + ") is already exist");
        }
        entityMap().put(t.getId(), t);
        return t;
    }

    @Override
    public T update(T t) {
        if (t.getId() == null || !entityMap().containsKey(t.getId())) {
            throw new UserNotFound("Entity with the same id(" + t.getId() + ") is not found.");
        }
        if (checkClassCast(t.getId())) {
            entityMap().put(t.getId(), t);
        }
        return t;
    }

    @Override
    public T get(long id) {
        if (!checkClassCast(id) || !entityMap().containsKey(id)) {
            return null;
        }
        return (T) entityMap().get(id);
    }

    @Override
    public boolean delete(long id) {
        return checkClassCast(id) && entityMap().remove(id) != null;
    }

    private Long maxId() {
        if (entityMap().isEmpty()) {
            return 0L;
        }
        long maxId = 0;
        for (Long id : EntityStorage.getEntities().keySet()) {
            if (id > maxId) maxId = id;
        }
        return maxId;
    }

    private Map<Long, Entity> entityMap() {
        return EntityStorage.getEntities();
    }

    private boolean checkClassCast(Long id) {
        if (entityMap().get(id) != null && !entityMap().get(id).getClass().isAssignableFrom(type)) {
            System.out.println("Error type casting");
            return false;
        }
        return true;
    }
}
