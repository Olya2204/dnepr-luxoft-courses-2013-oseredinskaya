package com.luxoft.dnepr.courses.unit2;


import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    private static final String NULL_VALUE_ERROR_MESSAGE = "Null value";
    private static final String EMPTY_STRING_ERROR_MESSAGE = "Empty string";

    public static String[] sortArray(String[] array, boolean asc) {
        if (array == null)
            throw new NullPointerException(NULL_VALUE_ERROR_MESSAGE);
        String[] mas = array.clone();
        if (mas.length <= 1) return mas;

        int mid = mas.length / 2;
        String[] left = new String[mid];
        String[] right = new String[mid + mas.length % 2];

        int j = 0;
        for (int i = 0; i < mas.length; i++) {
            if (i < mas.length / 2) {
                left[i] = mas[i];
            } else {
                right[j++] = mas[i];
            }
        }
        return merge(sortArray(left, asc), sortArray(right, asc), asc);
    }

    private static String[] merge(String[] left, String[] right, boolean asc) {
        int a = 0, b = 0;
        String[] merged = new String[left.length + right.length];
        for (int i = 0; i < left.length + right.length; i++) {
            if (b < right.length && a < left.length)
                if ((compareOfElements(left[a], right[b], asc)) && (b < right.length)) {
                    merged[i] = right[b++];
                } else {
                    merged[i] = left[a++];
                }
            else if (b < right.length)
                merged[i] = right[b++];
            else
                merged[i] = left[a++];
        }
        return merged;
    }

    private static boolean compareOfElements(String prev, String next, boolean asc) {
        return (prev.compareTo(next) > 0) == asc;
    }

    public static double wordAverageLength(String str) {
        if (str == null) throw new NullPointerException(NULL_VALUE_ERROR_MESSAGE);
        if (str.isEmpty()) return 0;
        double result = 0;
        String[] arrayOfString = str.split(" ");
        for (String myString : arrayOfString) {
            result = result + myString.length();
        }

        result = result / arrayOfString.length;
        return Math.rint(100.0 * result) / 100.0;
    }

    public static String reverseWords(String str) {
        if (str == null) throw new NullPointerException(NULL_VALUE_ERROR_MESSAGE);
        if (str.isEmpty()) return EMPTY_STRING_ERROR_MESSAGE;

        StringBuilder myString = new StringBuilder();
        String[] words = str.split(" ");
        for (String st : words) {
            for (int i = st.length() - 1; i >= 0; i--) {
                myString.append(st.charAt(i));
            }
            str = str.replaceAll(st, myString.toString());
            if (st.length() == 1) myString.deleteCharAt(0);
            else myString.delete(0, st.length());
        }
        return str;
    }

    public static char[] getCharEntries(String str) {
        if (str == null) throw new NullPointerException(NULL_VALUE_ERROR_MESSAGE);
        if (str.isEmpty()) throw new IllegalArgumentException("Empty string");
        char[] chars = str.toCharArray();
        int i = 0;
        Map<Character, Integer> ch = new TreeMap<Character, Integer>();

        for (Character symbol : chars) {
            if (!symbol.equals(' ')) {
                if (ch.containsKey(symbol)) {
                    ch.put(symbol, ch.get(symbol) + 1);
                    continue;
                }
                ch.put(symbol, 1);
            }
        }

        char[] result = new char[entriesSorted(ch).size()];
        for (Map.Entry<Character, Integer> entry : entriesSorted(ch)) {
            result[i] = entry.getKey();
            i++;

        }

        return result;
    }

    private static SortedSet<Map.Entry<Character, Integer>> entriesSorted(Map<Character, Integer> map) {
        SortedSet<Map.Entry<Character, Integer>> sortedEntries = new TreeSet<Map.Entry<Character, Integer>>(
                new Comparator<Map.Entry<Character, Integer>>() {
                    public int compare(Map.Entry<Character, Integer> e1, Map.Entry<Character, Integer> e2) {

                        if (e1.getValue().compareTo(e2.getValue()) > 0) return -1;
                        if (e1.getValue().compareTo(e2.getValue()) < 0) return 1;
                        return e1.getKey().compareTo(e2.getKey());
                    }
                }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    public static double calculateOverallArea(List<Figure> figures) {
        if (figures == null) throw new NullPointerException(NULL_VALUE_ERROR_MESSAGE);

        double S = 0;
        for (Figure myFigures : figures) {
            S = S + myFigures.calculateArea();
        }
        return S;
    }

}
