package com.luxoft.dnepr.courses.regular.unit5.exception;

public class UserAlreadyExist extends RuntimeException {

    public UserAlreadyExist() {

    }

    public UserAlreadyExist(String message) {
        super(message);
    }
}
