package com.luxoft.dnepr.courses.regular.unit7;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {
    final int FILE_QUEUE_SIZE = 10;

    private final String rootFolder;
    private final int maxNumberOfThreads;
    private final WordStorage wordStorage = new WordStorage();

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        BlockingQueue<File> sharedQueue = new ArrayBlockingQueue<>(FILE_QUEUE_SIZE);
        List<File> processedFiles = new CopyOnWriteArrayList<>();
        List<Thread> threads = new ArrayList<>();
        Producer producer = new Producer(sharedQueue, new File(rootFolder));
        Thread thread = new Thread(producer);
        threads.add(thread);
        thread.start();
        for (int i = 1; i < maxNumberOfThreads; i++) {
            thread = new Thread(new Consumer(sharedQueue, wordStorage, processedFiles));
            threads.add(thread);
            thread.start();
        }
        waitAllTreads(threads);
        return new FileCrawlerResults(processedFiles, wordStorage.getWordStatistics());
    }

    private void waitAllTreads(List<Thread> threads) {
        for (Thread tread : threads) {
            try {
                tread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}
