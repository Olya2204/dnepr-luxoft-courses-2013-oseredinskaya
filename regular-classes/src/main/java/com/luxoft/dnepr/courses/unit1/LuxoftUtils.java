package com.luxoft.dnepr.courses.unit1;


public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    /**
     * Function, which in the order of a month returns
     * its name in Russian or English.
     */

    public static String getMonthName(int monthOrder, String language) {
        if (language == null) return "Unknown Language";
        switch (monthOrder) {
            case 1:
                if (language.equals("ru")) return "Январь";
                if (language.equals("en")) return "January";
            case 2:
                if (language.equals("ru")) return "Февраль";
                if (language.equals("en")) return "February";
            case 3:
                if (language.equals("ru")) return "Март";
                if (language.equals("en")) return "March";
            case 4:
                if (language.equals("ru")) return "Апрель";
                if (language.equals("en")) return "April";
            case 5:
                if (language.equals("ru")) return "Май";
                if (language.equals("en")) return "May";
            case 6:
                if (language.equals("ru")) return "Июнь";
                if (language.equals("en")) return "June";
            case 7:
                if (language.equals("ru")) return "Июль";
                if (language.equals("en")) return "July";
            case 8:
                if (language.equals("ru")) return "Август";
                if (language.equals("en")) return "August";
            case 9:
                if (language.equals("ru")) return "Сентябрь";
                if (language.equals("en")) return "September";
            case 10:
                if (language.equals("ru")) return "Октябрь";
                if (language.equals("en")) return "October";
            case 11:
                if (language.equals("ru")) return "Ноябрь";
                if (language.equals("en")) return "November";
            case 12:
                if (language.equals("ru")) return "Декабрь";
                if (language.equals("en")) return "December";
            default:
                if (language.equals("ru")) return "Неизвестный месяц";
                if (language.equals("en")) return "Unknown month";

        }
        return "Unknown Language";

    }

    /**
     * Function, which returns representation of decimal number in the binary.
     */

    public static String decimalToBinary(String decimalNumber) {
        if ((decimalNumber == null) || decimalNumber.equals("")) return "Not decimal";

        for (int j = 0; j < decimalNumber.length(); j++)
            switch (decimalNumber.charAt(j)) {
                case '0':
                    break;
                case '1':
                    break;
                case '2':
                    break;
                case '3':
                    break;
                case '4':
                    break;
                case '5':
                    break;
                case '6':
                    break;
                case '7':
                    break;
                case '8':
                    break;
                case '9':
                    break;
                default:
                    return "Not decimal";
            }
        int temporaryVariable, decimalValue;
        StringBuilder buffer = new StringBuilder();
        decimalValue = Integer.parseInt(decimalNumber);
        while (decimalValue != 0) {
            temporaryVariable = decimalValue % 2;
            buffer.append(temporaryVariable);
            decimalValue = decimalValue / 2;
        }

        return buffer.reverse().toString();
    }

    /**
     * Function, which returns representation of binary number in the decimal.
     */

    public static String binaryToDecimal(String binaryNumber) {
        if ((binaryNumber == null) || binaryNumber.equals("")) return "Not binary";

        for (int j = 0; j < binaryNumber.length(); j++)
            switch (binaryNumber.charAt(j)) {
                case '0':
                    break;
                case '1':
                    break;
                default:
                    return "Not binary";
            }

        int result = 0;
        int binNum;
        for (int i = 0; i < binaryNumber.length(); i++) {

            if (binaryNumber.charAt(i) == '1') binNum = 1;
            else binNum = 0;
            result = result * 2 + binNum;
        }

        return Integer.toString(result);
    }

    /**
     * Function, which returns the sorted array of numbers
     * in a decreasing or increasing order.
     */

    public static int[] sortArray(int[] array, boolean asc) {
        if (array == null)
            throw new NullPointerException("Null value");

        int[] myArray1 = new int[array.length];
        System.arraycopy(array, 0, myArray1, 0, array.length);
        for (int i = 0; i < array.length; i++)
            for (int j = array.length - 1; j > i; j--)
                if (myArray1[j] < myArray1[j - 1]) {
                    int myVariable = myArray1[j];
                    myArray1[j] = myArray1[j - 1];
                    myArray1[j - 1] = myVariable;
                }

        if (asc) return myArray1;

        else {
            for (int k = 0; k < myArray1.length / 2; k++) {
                int myVariable = myArray1[myArray1.length - k - 1];
                myArray1[myArray1.length - k - 1] = myArray1[k];
                myArray1[k] = myVariable;
            }
            return myArray1;
        }
    }
}
