package com.luxoft.dnepr.courses.regular.unit14;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;

public class Vocabulary {
    private static final int MIN_WORD_LENGTH = 3;
    private static final String SOURCE_FILENAME = "/unit14/sonnets.txt";
    private HashSet<String> vocabulary = new HashSet<>();

    public Vocabulary() {
        addToVocabulary();
    }

    public HashSet<String> getVocabulary() {
        return vocabulary;
    }

    private String readFile() {
        String result = null;
        File source = new File(getClass().getResource(SOURCE_FILENAME).getFile());
        try {
            result = new Scanner(source).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            System.out.println("There were no space for something important in this tiny world. Your file not found.");
        }
        return result;
    }

    private void addToVocabulary() {
        String[] words = readFile().split("\\W+");
        for (String word : words) {
            if (word.length() > MIN_WORD_LENGTH) {
                vocabulary.add(word.toLowerCase());
            }
        }
    }

    public String getRandomWord() {
        return (String) vocabulary.toArray()[(int) (Math.random() * vocabulary.size())];
    }
}
