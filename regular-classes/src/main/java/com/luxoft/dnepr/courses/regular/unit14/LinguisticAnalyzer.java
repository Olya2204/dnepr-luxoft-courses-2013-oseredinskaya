package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;

public class LinguisticAnalyzer {

    public static void main(String[] args) {
        LinguisticAnalyzer ui = new LinguisticAnalyzer();
        System.out.println("Linguistic analyzer, author Tushar Brahmacobalol. Enter the help command to see the help menu");
        ui.execute();
    }

    public void execute() {
        String word, userCommand;
        Vocabulary vocabulary = new Vocabulary();
        Scanner scanner = new Scanner(System.in);
        do {
            word = vocabulary.getRandomWord();
            System.out.println("\nDo you know translation of this word?:");
            System.out.println(word);
            userCommand = scanner.next();
            commandHandler(userCommand, word);
        } while (!userCommand.equals("exit"));
        Words.estimatedVocabulary(vocabulary.getVocabulary().size());
    }

    private void commandHandler(String userCommand, String word) {
        switch (userCommand.toLowerCase()) {
            case "help":
                showHelp();
                break;
            case "yes":
                Words.addToMyVocabulary(word, true);
                break;
            case "no":
                Words.addToMyVocabulary(word, false);
                break;
            case "exit":
                break;
            default:
                System.out.println("List of available commands: yes, no, help, exit");
        }
    }

    private void showHelp() {
        System.out.println("Use YES/NO for answers.\nEXIT to know your estimated vocabulary and quit.\nHELP will show this help.");
    }

}