package com.luxoft.dnepr.courses.regular.unit6.familytree;

import java.io.Serializable;

public interface Person extends Serializable {

    String getName();

    String getEthnicity();

    Person getFather();

    Person getMother();

    Gender getGender();

    int getAge();

}
