package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    public final static File STOP = new File("");
    private final BlockingQueue<File> sharedQueue;
    private final File rootDirectory;

    public Producer(BlockingQueue<File> sharedQueue, File rootDirectory) {
        this.sharedQueue = sharedQueue;
        this.rootDirectory = rootDirectory;
    }

    @Override
    public void run() {
        find(rootDirectory);
        try {
            sharedQueue.put(STOP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void find(File rootFolder) {
        File[] files = rootFolder.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    find(file);
                } else {
                    if (file.getName().endsWith(".txt")) {
                        try {
                            sharedQueue.put(file);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
