package com.luxoft.dnepr.courses.regular.unit4;


import java.util.*;

public class EqualSet<E> implements Set<E> {
    private static final String NULL_VALUE_ERROR_MESSAGE = "Input parameter is null";

    List<E> set = new ArrayList<E>();

    public EqualSet() {
    }

    public EqualSet(Collection<? extends E> collection) {
        nullValueInput(collection);
        addAll(collection);
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Override
    public boolean contains(Object object) {
        return set.contains(object);
    }

    @Override
    public Iterator<E> iterator() {
        return set.iterator();
    }

    @Override
    public Object[] toArray() {
        return set.toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        if (array == null) {
            throw new IllegalArgumentException(NULL_VALUE_ERROR_MESSAGE);
        }
        return set.toArray(array);
    }

    @Override
    public boolean add(E element) {
        if (isEmpty()) {
            set.add(element);
            return true;
        }
        return !set.contains(element) && set.add(element);
    }

    @Override
    public boolean remove(Object object) {
        if (contains(object)) {
            set.remove(object);
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        nullValueInput(collection);
        return set.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        nullValueInput(collection);
        boolean result = false;
        for (E element : collection) {
            if (add(element)) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        nullValueInput(collection);
        return set.retainAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        nullValueInput(collection);
        return set.removeAll(collection);
    }

    @Override
    public void clear() {
        set.clear();
    }

    private void nullValueInput(Collection<?> collection) {
        if (collection == null) {
            throw new IllegalArgumentException(NULL_VALUE_ERROR_MESSAGE);
        }
    }

}
