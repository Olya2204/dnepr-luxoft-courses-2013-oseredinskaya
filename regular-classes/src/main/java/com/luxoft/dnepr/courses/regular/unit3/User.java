package com.luxoft.dnepr.courses.regular.unit3;


public class User implements UserInterface {
    private Long id;
    private String name;
    private WalletInterface wallet;

    public User() {
    }

    public User(Long id, String name, WalletInterface wallet) {
        this.id = id;
        this.name = name;
        this.wallet = wallet;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WalletInterface getWallet() {
        return wallet;
    }

    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }
}
