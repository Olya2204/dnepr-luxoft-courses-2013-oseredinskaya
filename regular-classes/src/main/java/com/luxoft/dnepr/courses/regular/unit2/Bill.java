package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private Map<Product, CompositeProduct> composedBillMap = new HashMap<Product, CompositeProduct>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        CompositeProduct productList = composedBillMap.get(product);
        if (productList == null) {
            productList = new CompositeProduct();
            composedBillMap.put(product, productList);
        }
        productList.add(product);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double sum = 0;
        for (CompositeProduct product : composedBillMap.values()) {
            sum = sum + product.getPrice();
        }

        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        List<Product> products = new ArrayList<Product>(composedBillMap.values());
        Collections.sort(products, new Comparator<Product>() {
            @Override
            public int compare(Product product1, Product product2) {
                return Double.compare(product2.getPrice(), product1.getPrice());
            }
        });
        return products;
    }


    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }


}

