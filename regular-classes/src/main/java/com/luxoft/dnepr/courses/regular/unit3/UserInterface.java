package com.luxoft.dnepr.courses.regular.unit3;

interface UserInterface {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    WalletInterface getWallet();

    void setWallet(WalletInterface wallet);

}
