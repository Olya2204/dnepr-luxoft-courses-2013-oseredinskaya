package com.luxoft.dnepr.courses.unit2.model;


public class Circle extends Figure {
    double radius;

    public Circle(double radius) {
        if (radius <= 0)
            throw new IllegalArgumentException(String.format("Input value '%s' is less than 0 or equals to zero", radius));
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return Math.PI * Math.pow(this.radius, 2);
    }
}
