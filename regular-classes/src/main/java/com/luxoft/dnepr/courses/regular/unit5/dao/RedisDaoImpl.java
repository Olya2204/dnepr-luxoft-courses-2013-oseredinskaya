package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.model.Redis;

public class RedisDaoImpl extends AbstractDao<Redis> {
    public RedisDaoImpl() {
        super(Redis.class);
    }
}
