package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;
import java.math.MathContext;

public class Wallet implements WalletInterface {

    private static final String WALLET_BLOCKED_ERROR_MESSAGE = "Wallet is blocked";
    private static final String INSUFFICIENT_FOUNDS_ERROR_MESSAGE = "Amount to withdraw is more,than amount in the wallet";
    private static final String LIMIT_EXCEEDED_ERROR_MESSAGE = "Wallet limit is exceeded";

    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet() {
    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, WALLET_BLOCKED_ERROR_MESSAGE);
        }

        if (amount.compareTo(amountToWithdraw) < 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount,
                    INSUFFICIENT_FOUNDS_ERROR_MESSAGE);

        }


    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, WALLET_BLOCKED_ERROR_MESSAGE);
        }

        if (amountToTransfer.add(amount).compareTo(maxAmount) > 0) {
            throw new LimitExceededException(id, amountToTransfer, amount, LIMIT_EXCEEDED_ERROR_MESSAGE);

        }

    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }
}
